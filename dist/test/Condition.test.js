"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var ConditionImpl_1 = require("../common/ConditionImpl");
var EqualsCondition_1 = require("../common/conditions/EqualsCondition");
var GreaterThanCondition_1 = require("../common/conditions/GreaterThanCondition");
var LessThanCondition_1 = require("../common/conditions/LessThanCondition");
function testCondProps(cond) {
    expect(cond.left).toBeDefined();
    expect(cond.right).toBeDefined();
    expect(cond.type).toBeDefined();
    expect(cond.checkTrue).toBeDefined();
    expect(cond.makeTrue).toBeDefined();
}
exports.testCondProps = testCondProps;
test("Condition factory methods create valid conditions.", function () { return __awaiter(_this, void 0, void 0, function () {
    var c1State, c1, c2State, c2, andCond, orCond, _a, _b, _c, _d, _e, _f;
    var _this = this;
    return __generator(this, function (_g) {
        switch (_g.label) {
            case 0:
                c1State = false;
                c1 = new ConditionImpl_1.default();
                c1.checkTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, c1State];
                }); }); };
                c1.makeTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    c1State = true;
                    return [2 /*return*/];
                }); }); };
                c2State = false;
                c2 = new ConditionImpl_1.default();
                c2.checkTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, c2State];
                }); }); };
                c2.makeTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    c2State = true;
                    return [2 /*return*/];
                }); }); };
                andCond = c1.and(c2);
                orCond = c1.or(c2);
                _a = expect;
                return [4 /*yield*/, andCond.checkTrue()];
            case 1:
                _a.apply(void 0, [_g.sent()]).toBe(false);
                _b = expect;
                return [4 /*yield*/, orCond.checkTrue()];
            case 2:
                _b.apply(void 0, [_g.sent()]).toBe(false);
                return [4 /*yield*/, c1.makeTrue()];
            case 3:
                _g.sent();
                _c = expect;
                return [4 /*yield*/, andCond.checkTrue()];
            case 4:
                _c.apply(void 0, [_g.sent()]).toBe(false);
                _d = expect;
                return [4 /*yield*/, orCond.checkTrue()];
            case 5:
                _d.apply(void 0, [_g.sent()]).toBe(true);
                return [4 /*yield*/, c2.makeTrue()];
            case 6:
                _g.sent();
                _e = expect;
                return [4 /*yield*/, andCond.checkTrue()];
            case 7:
                _e.apply(void 0, [_g.sent()]).toBe(true);
                _f = expect;
                return [4 /*yield*/, orCond.checkTrue()];
            case 8:
                _f.apply(void 0, [_g.sent()]).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
test("EqualsCondition", function () { return __awaiter(_this, void 0, void 0, function () {
    var v1State, v1, v2State, v2, c, _a, _b;
    var _this = this;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                v1State = 1;
                v1 = {
                    get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        return [2 /*return*/, v1State];
                    }); }); },
                    set: function (v) {
                        if (v)
                            v1State = v;
                    }
                };
                v2State = 2;
                v2 = {
                    get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        return [2 /*return*/, v2State];
                    }); }); },
                    set: function (v) {
                        if (v)
                            v2State = v;
                    }
                };
                c = new EqualsCondition_1.default({
                    left: v1,
                    right: v2
                });
                _a = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 1:
                _a.apply(void 0, [_c.sent()]).toBe(false);
                return [4 /*yield*/, c.makeTrue()];
            case 2:
                _c.sent();
                expect(v1State).toBe(2);
                expect(v2State).toBe(2);
                _b = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 3:
                _b.apply(void 0, [_c.sent()]).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
test("LessThanCondition", function () { return __awaiter(_this, void 0, void 0, function () {
    var v1State, v1, v2State, v2, c, _a, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                v1State = 1;
                v1 = {
                    get: function (v) { return v1State; }
                };
                v2State = 2;
                v2 = {
                    get: function () { return v2State; }
                };
                c = new LessThanCondition_1.default({
                    left: v1,
                    right: v2
                });
                _a = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 1:
                _a.apply(void 0, [_c.sent()]).toBe(true);
                v2State = 1;
                _b = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 2:
                _b.apply(void 0, [_c.sent()]).toBe(false);
                return [2 /*return*/];
        }
    });
}); });
test("GreaterThanCondition", function () { return __awaiter(_this, void 0, void 0, function () {
    var v1State, v1, v2State, v2, c, _a, _b;
    var _this = this;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                v1State = 1;
                v1 = {
                    get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        return [2 /*return*/, v1State];
                    }); }); },
                    set: function (v) { }
                };
                v2State = 1;
                v2 = {
                    get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                        return [2 /*return*/, v2State];
                    }); }); },
                    set: function (v) { }
                };
                c = new GreaterThanCondition_1.default({
                    left: v1,
                    right: v2
                });
                _a = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 1:
                _a.apply(void 0, [_c.sent()]).toBe(false);
                v2State = 0;
                _b = expect;
                return [4 /*yield*/, c.checkTrue()];
            case 2:
                _b.apply(void 0, [_c.sent()]).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
