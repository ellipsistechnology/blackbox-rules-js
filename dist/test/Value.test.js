"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var ValueImpl_1 = require("../common/ValueImpl");
var ConstantValue_1 = require("../common/values/ConstantValue");
var DateTimeValue_1 = require("../common/values/DateTimeValue");
var RemoteValue_1 = require("../common/values/RemoteValue");
var VariableValue_1 = require("../common/values/VariableValue");
var Condition_test_1 = require("./Condition.test");
exports.makeRemoteValueProtocol = function (remoteValues) {
    return {
        lookupAgentValueAddress: function (agentAddress) {
            return agentAddress + "/value";
        },
        setValue: function (agentValueAddress, remoteValueName, value) { return __awaiter(_this, void 0, void 0, function () {
            var agentAddress;
            return __generator(this, function (_a) {
                agentAddress = agentValueAddress.split('/')[0];
                remoteValues[agentAddress][remoteValueName] = value;
                return [2 /*return*/, null];
            });
        }); },
        getValue: function (agentValueAddress, remoteValueName) { return __awaiter(_this, void 0, void 0, function () {
            var agentAddress;
            return __generator(this, function (_a) {
                agentAddress = agentValueAddress.split('/')[0];
                return [2 /*return*/, remoteValues[agentAddress][remoteValueName]];
            });
        }); }
    };
};
test('Value factory methods create valid conditions.', function () { return __awaiter(_this, void 0, void 0, function () {
    var v1, v2, eqCond, ltCond, gtCond, _a, _b, _c, eqCond2, ltCond2, gtCond2, _d, _e, _f, eqCond3, ltCond3, gtCond3, _g, _h, _j;
    var _this = this;
    return __generator(this, function (_k) {
        switch (_k.label) {
            case 0:
                v1 = new ValueImpl_1.default();
                v2 = new ValueImpl_1.default();
                v1.get = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, 1];
                }); }); };
                v2.get = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, 1
                        // Test when equal:
                    ];
                }); }); };
                eqCond = v1.eq(v2);
                ltCond = v1.lt(v2);
                gtCond = v1.gt(v2);
                Condition_test_1.testCondProps(eqCond);
                Condition_test_1.testCondProps(ltCond);
                Condition_test_1.testCondProps(gtCond);
                _a = expect;
                return [4 /*yield*/, eqCond.checkTrue()];
            case 1:
                _a.apply(void 0, [_k.sent()]).toBe(true);
                _b = expect;
                return [4 /*yield*/, ltCond.checkTrue()];
            case 2:
                _b.apply(void 0, [_k.sent()]).toBe(false);
                _c = expect;
                return [4 /*yield*/, gtCond.checkTrue()];
            case 3:
                _c.apply(void 0, [_k.sent()]).toBe(false);
                v2.get = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, 2
                        // Test when v1 < v2:
                    ];
                }); }); };
                eqCond2 = v1.eq(v2);
                ltCond2 = v1.lt(v2);
                gtCond2 = v1.gt(v2);
                _d = expect;
                return [4 /*yield*/, eqCond2.checkTrue()];
            case 4:
                _d.apply(void 0, [_k.sent()]).toBe(false);
                _e = expect;
                return [4 /*yield*/, ltCond2.checkTrue()];
            case 5:
                _e.apply(void 0, [_k.sent()]).toBe(true);
                _f = expect;
                return [4 /*yield*/, gtCond2.checkTrue()];
            case 6:
                _f.apply(void 0, [_k.sent()]).toBe(false);
                // Test when v1 > v2:
                v2.get = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/, 0];
                }); }); };
                eqCond3 = v1.eq(v2);
                ltCond3 = v1.lt(v2);
                gtCond3 = v1.gt(v2);
                _g = expect;
                return [4 /*yield*/, eqCond3.checkTrue()];
            case 7:
                _g.apply(void 0, [_k.sent()]).toBe(false);
                _h = expect;
                return [4 /*yield*/, ltCond3.checkTrue()];
            case 8:
                _h.apply(void 0, [_k.sent()]).toBe(false);
                _j = expect;
                return [4 /*yield*/, gtCond3.checkTrue()];
            case 9:
                _j.apply(void 0, [_k.sent()]).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
test('ConstantValue', function () { return __awaiter(_this, void 0, void 0, function () {
    var constValue, _a, v, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                constValue = new ConstantValue_1.default(undefined);
                expect(constValue.type).toBe(ConstantValue_1.CONSTANT_TYPE);
                _a = expect;
                return [4 /*yield*/, constValue.get()];
            case 1:
                _a.apply(void 0, [_c.sent()]).toBeUndefined();
                v = 123.456;
                constValue.set(v);
                _b = expect;
                return [4 /*yield*/, constValue.get()];
            case 2:
                _b.apply(void 0, [_c.sent()]).toBe(v);
                return [2 /*return*/];
        }
    });
}); });
test('DateTimeValue', function () { return __awaiter(_this, void 0, void 0, function () {
    var dateTimeValue, _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                dateTimeValue = new DateTimeValue_1.default();
                expect(dateTimeValue.type).toBe(DateTimeValue_1.DATE_TIME_TYPE);
                _a = expect;
                return [4 /*yield*/, dateTimeValue.get()];
            case 1:
                _a.apply(void 0, [_d.sent()]).toBeInstanceOf(Date);
                _b = expect;
                _c = new Date().getTime();
                return [4 /*yield*/, dateTimeValue.get()];
            case 2:
                _b.apply(void 0, [_c - (_d.sent()).getTime()]).toBeLessThan(1); // 1 millisecond precision
                return [2 /*return*/];
        }
    });
}); });
exports.remoteValues = {
    agent1: {
        value1: 1,
        value2: 2
    },
    agent2: {
        value1: 3
    }
};
exports.testRemoteValue = function (createRemoteValue) { return __awaiter(_this, void 0, void 0, function () {
    var agent1Value1Address, agent1Value2Address, agent2Value1Address, remoteValue11, remoteValue12, remoteValue21, _a, _b, _c, v11, v12, v21, _d, _e, _f;
    return __generator(this, function (_g) {
        switch (_g.label) {
            case 0:
                agent1Value1Address = "agent1/value/value1";
                agent1Value2Address = "agent1/value/value2";
                agent2Value1Address = "agent2/value/value1";
                remoteValue11 = createRemoteValue(agent1Value1Address, "value1");
                remoteValue12 = createRemoteValue(agent1Value2Address, "value2");
                remoteValue21 = createRemoteValue(agent2Value1Address, "value1");
                expect(remoteValue11.type).toBe(RemoteValue_1.REMOTE_TYPE);
                expect(remoteValue12.type).toBe(RemoteValue_1.REMOTE_TYPE);
                expect(remoteValue21.type).toBe(RemoteValue_1.REMOTE_TYPE);
                _a = expect;
                return [4 /*yield*/, remoteValue11.get()];
            case 1:
                _a.apply(void 0, [_g.sent()]).toBe(exports.remoteValues.agent1.value1);
                _b = expect;
                return [4 /*yield*/, remoteValue12.get()];
            case 2:
                _b.apply(void 0, [_g.sent()]).toBe(exports.remoteValues.agent1.value2);
                _c = expect;
                return [4 /*yield*/, remoteValue21.get()];
            case 3:
                _c.apply(void 0, [_g.sent()]).toBe(exports.remoteValues.agent2.value1);
                v11 = 123.456;
                v12 = -11;
                v21 = 12;
                remoteValue11.set(v11);
                remoteValue12.set(v12);
                remoteValue21.set(v21);
                expect(exports.remoteValues.agent1.value1).toBe(v11);
                expect(exports.remoteValues.agent1.value2).toBe(v12);
                expect(exports.remoteValues.agent2.value1).toBe(v21);
                _d = expect;
                return [4 /*yield*/, remoteValue11.get()];
            case 4:
                _d.apply(void 0, [_g.sent()]).toBe(v11);
                _e = expect;
                return [4 /*yield*/, remoteValue12.get()];
            case 5:
                _e.apply(void 0, [_g.sent()]).toBe(v12);
                _f = expect;
                return [4 /*yield*/, remoteValue21.get()];
            case 6:
                _f.apply(void 0, [_g.sent()]).toBe(v21);
                return [2 /*return*/];
        }
    });
}); };
test('RemoteValue', function () { return __awaiter(_this, void 0, void 0, function () {
    var remoteValueProtocol;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                remoteValueProtocol = exports.makeRemoteValueProtocol(exports.remoteValues);
                return [4 /*yield*/, exports.testRemoteValue(function (agentValueAddress, remoteValueName) {
                        var rv = new RemoteValue_1.default(remoteValueProtocol);
                        rv.remoteValueAddress = agentValueAddress;
                        rv.remoteValueName = remoteValueName;
                        return rv;
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
exports.makeVariableStore = function () {
    var vars = {};
    return {
        get: function (variableName) { return vars[variableName]; },
        put: function (variableName, v) { return vars[variableName] = v; }
    };
};
exports.testVariableValue = function (makeVariableValue) { return __awaiter(_this, void 0, void 0, function () {
    var variableValue1, variableValue2, variableValue3, _a, _b, _c, v1, v2, v3, _d, _e, _f, _g, _h, _j, _k, _l, _m;
    return __generator(this, function (_o) {
        switch (_o.label) {
            case 0:
                variableValue1 = makeVariableValue("v1");
                variableValue2 = makeVariableValue("v2");
                variableValue3 = makeVariableValue("v3");
                expect(variableValue1.type).toBe(VariableValue_1.VARIABLE_TYPE);
                expect(variableValue2.type).toBe(VariableValue_1.VARIABLE_TYPE);
                expect(variableValue3.type).toBe(VariableValue_1.VARIABLE_TYPE);
                _a = expect;
                return [4 /*yield*/, variableValue1.get()];
            case 1:
                _a.apply(void 0, [_o.sent()]).toBeUndefined();
                _b = expect;
                return [4 /*yield*/, variableValue2.get()];
            case 2:
                _b.apply(void 0, [_o.sent()]).toBeUndefined();
                _c = expect;
                return [4 /*yield*/, variableValue3.get()];
            case 3:
                _c.apply(void 0, [_o.sent()]).toBeUndefined();
                v1 = 111;
                v2 = 222;
                v3 = 333;
                variableValue1.set(v1);
                _d = expect;
                return [4 /*yield*/, variableValue1.get()];
            case 4:
                _d.apply(void 0, [_o.sent()]).toBe(v1);
                variableValue2.set(v2);
                _e = expect;
                return [4 /*yield*/, variableValue1.get()];
            case 5:
                _e.apply(void 0, [_o.sent()]).toBe(v1);
                _f = expect;
                return [4 /*yield*/, variableValue2.get()];
            case 6:
                _f.apply(void 0, [_o.sent()]).toBe(v2);
                variableValue3.set(v3);
                _g = expect;
                return [4 /*yield*/, variableValue1.get()];
            case 7:
                _g.apply(void 0, [_o.sent()]).toBe(v1);
                _h = expect;
                return [4 /*yield*/, variableValue2.get()];
            case 8:
                _h.apply(void 0, [_o.sent()]).toBe(v2);
                _j = expect;
                return [4 /*yield*/, variableValue3.get()];
            case 9:
                _j.apply(void 0, [_o.sent()]).toBe(v3);
                variableValue1.set(v2);
                variableValue2.set(v3);
                variableValue3.set(v1);
                _k = expect;
                return [4 /*yield*/, variableValue1.get()];
            case 10:
                _k.apply(void 0, [_o.sent()]).toBe(v2);
                _l = expect;
                return [4 /*yield*/, variableValue2.get()];
            case 11:
                _l.apply(void 0, [_o.sent()]).toBe(v3);
                _m = expect;
                return [4 /*yield*/, variableValue3.get()];
            case 12:
                _m.apply(void 0, [_o.sent()]).toBe(v1);
                return [2 /*return*/];
        }
    });
}); };
test('VariableValue', function () { return __awaiter(_this, void 0, void 0, function () {
    var variableStore;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                variableStore = exports.makeVariableStore();
                return [4 /*yield*/, exports.testVariableValue(function (name) {
                        var vv = new VariableValue_1.default(variableStore);
                        vv.variableName = name;
                        return vv;
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
