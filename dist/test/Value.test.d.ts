export declare const makeRemoteValueProtocol: (remoteValues: any) => {
    lookupAgentValueAddress: (agentAddress: any) => string;
    setValue: (agentValueAddress: string, remoteValueName: string, value: any) => Promise<any>;
    getValue: (agentValueAddress: string, remoteValueName: string) => Promise<any>;
};
export declare const remoteValues: {
    agent1: {
        value1: number;
        value2: number;
    };
    agent2: {
        value1: number;
    };
};
export declare const testRemoteValue: (createRemoteValue: any) => Promise<void>;
export declare const makeVariableStore: () => {
    get: (variableName: any) => any;
    put: (variableName: any, v: any) => any;
};
export declare const testVariableValue: (makeVariableValue: any) => Promise<void>;
