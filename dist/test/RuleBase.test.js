"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var RuleBase_1 = require("../common/RuleBase");
var Value_test_1 = require("./Value.test");
var ConditionImpl_1 = require("../common/ConditionImpl");
var Rule_1 = require("../common/Rule");
var __1 = require("..");
// TODO test locking of rule
function makeStore() {
    var conditions = {};
    var rules = {};
    var values = {};
    return {
        getCondition: function (name) { return conditions[name]; },
        putCondition: function (cond) { if (cond)
            conditions[cond.name] = cond; },
        removeCondition: function (name) { return conditions[name] = undefined; },
        conditions: conditions,
        allConditions: function () { return Object.values(conditions); },
        getRule: function (name) { return rules[name]; },
        putRule: function (rule) { if (rule)
            rules[rule.name] = rule; },
        removeRule: function (name) { return rules[name] = undefined; },
        rules: rules,
        allRules: function () { return Object.values(rules); },
        getValue: function (name) { return values[name]; },
        putValue: function (value) { if (value)
            values[value.name] = value; },
        removeValue: function (name) { return values[name] = undefined; },
        values: rules,
        allValues: function () { return Object.values(rules); }
    };
}
test("Rule store can store, retrieve and delete conditions", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var name = "test condition ~!@#$%^&*()`[]\\{}|:\";'<>?,./'";
    var c = new ConditionImpl_1.default();
    c.name = name;
    rb.addCondition(c);
    expect(rb.store.conditions[name]).toBeDefined();
    var readCond = rb.getCondition(name);
    expect(readCond).toBe(c);
    rb.removeCondition(name);
    expect(rb.store.conditions[name]).toBeUndefined();
    var readCond2 = rb.getCondition(name);
    expect(readCond2).toBeUndefined();
});
test("Rule store can store, retrieve and delete rules", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var name = "test rule ~!@#$%^&*()`[]\\{}|:\";'<>?,./'";
    var r = new Rule_1.default({
        name: name,
        action: new ConditionImpl_1.default({ name: "a" }),
        trigger: new ConditionImpl_1.default({ name: "t" })
    });
    rb.addRule(r);
    expect(rb.store.rules[name]).toBeDefined();
    var readRule = rb.getRule(name);
    expect(readRule).toBe(r);
    rb.removeRule(name);
    expect(rb.store.rules[name]).toBeUndefined();
    var readRule2 = rb.getRule(name);
    expect(readRule2).toBeUndefined();
});
test("Rules can't be added without a valid name, trigger and action", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var r = {};
    expect(function () { return rb.addRule(r); }).toThrow();
    r.name = "";
    expect(function () { return rb.addRule(r); }).toThrow();
    r.name = "test";
    expect(function () { return rb.addRule(r); }).toThrow();
    r.trigger = { name: "t" };
    expect(function () { return rb.addRule(r); }).toThrow();
    r.action = { name: "a" };
    rb.addRule(r); // Should add without error
    // Missing name:
    var r1 = new Rule_1.default({
        trigger: new ConditionImpl_1.default({ name: "t" }),
        action: new ConditionImpl_1.default({ name: "a" })
    });
    expect(function () { return rb.addRule(r1); }).toThrow();
    // Invalid name:
    var r2 = new Rule_1.default({
        name: "",
        trigger: { name: "t" },
        action: { name: "a" }
    });
    expect(function () { return rb.addRule(r2); }).toThrow();
    // Missing action:
    var r3 = new Rule_1.default({
        name: "bob",
        trigger: { name: "t" }
    });
    expect(function () { return rb.addRule(r3); }).toThrow();
    // Missing trigger:
    var r4 = new Rule_1.default({
        name: "bob",
        action: { name: "a" }
    });
    expect(function () { return rb.addRule(r4); }).toThrow();
    // Missing trigger name:
    var r5 = new Rule_1.default({
        name: "bob",
        action: { name: "a" },
        trigger: {}
    });
    expect(function () { return rb.addRule(r5); }).toThrow();
    // Missing action name:
    var r6 = new Rule_1.default({
        name: "bob",
        action: {},
        trigger: { name: "t" }
    });
    expect(function () { return rb.addRule(r6); }).toThrow();
});
test("Rules can't be added with an invalid pre or post condition", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var r1 = {
        name: "test1",
        preCondition: {},
        trigger: { name: "trigger" },
        action: { name: "action" }
    };
    expect(function () { return rb.addRule(r1); }).toThrow(); // invalid preCondition
    r1.preCondition.name = "pre";
    rb.addRule(r1); // Should add without error
    var r2 = {
        name: "test2",
        preCondition: { name: "pre" },
        trigger: { name: "trigger" },
        action: { name: "action" }
    };
    r2.postCondition = new ConditionImpl_1.default();
    expect(function () { return rb.addRule(r2); }).toThrow(); // invalid postCondtion
    r2.postCondition.name = "post";
    rb.addRule(r2); // Should add without error
});
test("Variable store can store, retrieve and delete", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rb = new RuleBase_1.default({
                    variableStore: Value_test_1.makeVariableStore(),
                    store: makeStore()
                });
                return [4 /*yield*/, Value_test_1.testVariableValue(function (name) { return rb.variableValue(name); })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
test("Remote value protocol can store, retrieve and delete", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rb = new RuleBase_1.default({
                    remoteValueProtocol: Value_test_1.makeRemoteValueProtocol(Value_test_1.remoteValues),
                    store: makeStore()
                });
                i = 0;
                return [4 /*yield*/, Value_test_1.testRemoteValue(function (agentValueAddress, remote) {
                        return rb.remoteValue(agentValueAddress, remote).withName("test" + i++);
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
test("Unconditional is unconditional", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb, uncond, _a, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                rb = new RuleBase_1.default({});
                uncond = rb.unconditional();
                _a = expect;
                return [4 /*yield*/, uncond.checkTrue()];
            case 1:
                _a.apply(void 0, [_c.sent()]).toBeTruthy();
                return [4 /*yield*/, uncond.makeTrue()];
            case 2:
                _c.sent();
                _b = expect;
                return [4 /*yield*/, uncond.checkTrue()];
            case 3:
                _b.apply(void 0, [_c.sent()]).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
test("Rules can be created with when(trigger).then(action).withName(name)", function () {
    var rb = new RuleBase_1.default({ store: makeStore() });
    rb.store.conditions.trigger = { name: "trigger", type: 't1' };
    rb.store.conditions.action = { name: "action", type: 't2' };
    var name = "rule name";
    var r = rb.when({ name: "trigger" }).then({ name: "action" }).withName(name);
    expect(r).toBeDefined();
    expect(r.trigger).toBeDefined();
    expect(r.action).toBeDefined();
    expect(r.name).toBeDefined();
    expect(r.trigger.name).toBe("trigger");
    expect(r.action.name).toBe("action");
    expect(r.trigger.type).toBe("t1");
    expect(r.action.type).toBe("t2");
    expect(r.name).toBe(name);
    expect(rb.getRule(name)).toBe(r);
});
test("All factory methods can be used together", function () {
    var rb = new RuleBase_1.default({ store: makeStore() });
    var r = rb.when(rb.constantValue('val').withName('test1')
        .eq(rb.constantValue('val').withName('test2'))
        .withName('trigger')).then(rb.constantValue('val').withName('test3')
        .eq(rb.constantValue('val').withName('test4'))
        .withName('action')).withName('test');
    expect(rb.getRule('test')).toBe(r);
});
function makeCondition(_a) {
    var name = _a.name, left = _a.left, right = _a.right, makeTrue = _a.makeTrue, checkTrue = _a.checkTrue;
    var c = new ConditionImpl_1.default({ left: left, right: right, name: name });
    if (makeTrue)
        c.makeTrue = makeTrue;
    if (checkTrue)
        c.checkTrue = checkTrue;
    return c;
}
test("Rules are processed after run period with interrupts", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb, count, calls;
    var _this = this;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rb = new RuleBase_1.default({
                    store: makeStore(),
                    runPeriod: 100
                });
                expect(rb.runPeriod).toBe(100);
                count = 0;
                calls = {};
                rb.addRule({
                    name: "test",
                    preCondition: makeCondition({
                        name: "pre",
                        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            calls.pre = count++;
                            return [2 /*return*/];
                        }); }); }
                    }),
                    trigger: makeCondition({
                        name: "trigger",
                        checkTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, true];
                        }); }); }
                    }),
                    action: makeCondition({
                        name: "action",
                        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            calls.act = count++;
                            return [2 /*return*/];
                        }); }); }
                    }),
                    postCondition: makeCondition({
                        name: "post",
                        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            calls.post = count++;
                            return [2 /*return*/];
                        }); }); }
                    })
                });
                rb.start();
                return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 200); })];
            case 1:
                _a.sent();
                expect(calls.pre).toBeDefined();
                expect(calls.post).toBeDefined();
                expect(calls.pre).toBeLessThan(calls.post);
                expect(calls.act).toBeDefined();
                expect(calls.act).toBeGreaterThan(0);
                calls.pre = calls.post = calls.act = 0; // reset
                return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 200); })];
            case 2:
                _a.sent();
                expect(calls.pre).toBeLessThan(calls.post);
                expect(calls.act).toBeGreaterThan(0);
                rb.stop();
                calls.pre = calls.post = calls.act = 0; // reset
                return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 200); })];
            case 3:
                _a.sent();
                expect(calls.pre).toEqual(0);
                expect(calls.post).toEqual(0);
                expect(calls.act).toEqual(0);
                return [2 /*return*/];
        }
    });
}); });
test("Rules are processed after run period", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb, count, calls, act, processed;
    var _this = this;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rb = new RuleBase_1.default({
                    store: makeStore(),
                    runPeriod: 0
                });
                expect(rb.runPeriod).toBe(0);
                count = 0;
                calls = {};
                act = jest.fn();
                rb.addRule({
                    name: "test",
                    preCondition: makeCondition({
                        name: "pre",
                        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            calls.pre = count++;
                            return [2 /*return*/];
                        }); }); }
                    }),
                    trigger: makeCondition({
                        name: "trigger",
                        checkTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            return [2 /*return*/, true];
                        }); }); }
                    }),
                    action: makeCondition({
                        name: "action",
                        makeTrue: act
                    }),
                    postCondition: makeCondition({
                        name: "post",
                        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            calls.post = count++;
                            return [2 /*return*/];
                        }); }); }
                    })
                });
                return [4 /*yield*/, rb.processRules()];
            case 1:
                processed = _a.sent();
                expect(processed).toBe(true);
                expect(calls.pre).toBeDefined();
                expect(calls.post).toBeDefined();
                expect(calls.pre).toBeLessThan(calls.post);
                expect(act).toHaveBeenCalledTimes(1);
                calls.pre = calls.post = 0; // reset
                rb.store.rules.test.trigger.checkTrue = function () { return false; };
                return [4 /*yield*/, rb.processRules()];
            case 2:
                processed = _a.sent();
                expect(processed).toBe(true);
                expect(calls.pre).toBeLessThan(calls.post);
                expect(act).toHaveBeenCalledTimes(1); // shouldn't have been called again
                return [2 /*return*/];
        }
    });
}); });
test("Rules are processed only after runPeriod", function () { return __awaiter(_this, void 0, void 0, function () {
    var rb, trig, run;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                rb = new RuleBase_1.default({
                    store: makeStore(),
                    runPeriod: 1000
                });
                trig = jest.fn();
                rb.addRule(new Rule_1.default({
                    name: "test",
                    trigger: makeCondition({ name: "trigger", checkTrue: trig }),
                    action: makeCondition({ name: "action" })
                }));
                return [4 /*yield*/, rb.processRules()];
            case 1:
                run = _a.sent();
                expect(run).toBe(true);
                expect(trig).toHaveBeenCalledTimes(1);
                return [4 /*yield*/, rb.processRules()];
            case 2:
                run = _a.sent();
                expect(run).toBe(false);
                expect(trig).toHaveBeenCalledTimes(1);
                // Simulate runPeriod passing by setting runPeriod to now-lastRun:
                rb.runPeriod = new Date().getTime() - rb.lastRun.getTime();
                return [4 /*yield*/, rb.processRules()];
            case 3:
                run = _a.sent();
                expect(run).toBe(true);
                expect(trig).toHaveBeenCalledTimes(2);
                return [2 /*return*/];
        }
    });
}); });
test("Values created with rulebase factory methods are stored in the rulebase.", function () {
    var rb = new RuleBase_1.default({
        store: makeStore(),
        remoteValueProtocol: Value_test_1.makeRemoteValueProtocol({}),
        variableStore: Value_test_1.makeVariableStore()
    });
    var rv = rb.remoteValue('test', 'test').withName('test1');
    expect(rb.getValue('test1')).toBe(rv);
    var cv = rb.constantValue('val').withName('test2');
    expect(rb.getValue('test2')).toBe(cv);
    var dtv = rb.dateTimeValue().withName('test3');
    expect(rb.getValue('test3')).toBe(dtv);
    var vv1 = rb.variableValue('var name').withName('test4');
    expect(rb.getValue('test4')).toBe(vv1);
    var vv2 = rb.variableValue().withName('test5');
    expect(rb.getValue('test5')).toBe(vv2);
    var lv = rb.logValue().withName('test6');
    expect(rb.getValue('test6')).toBe(lv);
});
test("Adding a rule adds conditions and values", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var rule = new Rule_1.default({
        name: 'test rule',
        trigger: new __1.EqualsCondition({
            name: 'trigger',
            left: { name: 'trigger.left', type: 'test', get: function () { return null; }, set: function () { } },
            right: { name: 'trigger.right', type: 'test', get: function () { return null; }, set: function () { } }
        }),
        action: new __1.EqualsCondition({
            name: 'action',
            left: { name: 'action.left', type: 'test', get: function () { return null; }, set: function () { } },
            right: { name: 'action.right', type: 'test', get: function () { return null; }, set: function () { } }
        })
    });
    rb.addRule(rule);
    expect(rb.getValue(rule.trigger.left.name))
        .toBe(rule.trigger.left);
    expect(rb.getValue(rule.trigger.right.name))
        .toBe(rule.trigger.right);
    expect(rb.getValue(rule.action.left.name))
        .toBe(rule.action.left);
    expect(rb.getValue(rule.action.right.name))
        .toBe(rule.action.right);
    expect(rb.getCondition(rule.trigger.name)).toBe(rule.trigger);
    expect(rb.getCondition(rule.action.name)).toBe(rule.action);
    expect(rb.getRule(rule.name)).toBe(rule);
});
test("Can't create value with same name as existing", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    rb.constantValue('val').withName('test');
    expect(function () { return rb.constantValue('val').withName('test'); }).toThrowError();
});
test("Can't create value without a name", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    expect(function () { return rb.constantValue('val').withName(''); }).toThrowError();
    expect(function () { return rb.constantValue('val').withName(null); }).toThrowError();
    expect(function () { return rb.constantValue('val').withName(undefined); }).toThrowError();
});
test("Can't create condition without a name", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var c1 = rb.constantValue('val').withName('test').eq(rb.constantValue('val').withName('test2'));
    expect(function () { return rb.addCondition(c1); }).toThrowError();
    var c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'));
    c2.name = '';
    expect(function () { return rb.addCondition(c2); }).toThrowError();
    var c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'));
    c2.name = null;
    expect(function () { return rb.addCondition(c3); }).toThrowError();
});
test("Can replace value", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    rb.constantValue('val').withName('test');
    var v = new __1.ValueImpl();
    v.name = 'test';
    v.type = 't1';
    rb.replaceValue(v);
    expect(rb.getValue('test')).toBe(v);
});
test("Can replace condition", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'));
    c1.name = 'test cond';
    rb.addCondition(c1);
    var c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'));
    c2.name = 'test cond';
    rb.replaceCondition(c2);
    expect(rb.getCondition('test cond')).toBe(c2);
    expect(rb.getValue('test3')).toBe(c2.left);
    expect(rb.getValue('test4')).toBe(c2.right);
});
test("Can replace rule", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'));
    c1.name = 'c1';
    var c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'));
    c2.name = 'c2';
    var r1 = rb.when(c1).then(c2).withName('test');
    var c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'));
    c3.name = 'c3';
    var c4 = rb.constantValue('val').withName('test7').eq(rb.constantValue('val').withName('test8'));
    c4.name = 'c4';
    var r2 = new Rule_1.default({
        name: 'test',
        trigger: c3,
        action: c4
    });
    rb.replaceRule(r2);
    expect(rb.getRule('test')).toBe(r2);
    expect(rb.getCondition('c3')).toBe(c3);
    expect(rb.getCondition('c4')).toBe(c4);
    expect(rb.getValue('test5')).toBe(c3.left);
    expect(rb.getValue('test6')).toBe(c3.right);
    expect(rb.getValue('test7')).toBe(c4.left);
    expect(rb.getValue('test8')).toBe(c4.right);
});
test("Can't add condtion with same name as existing", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var c1 = rb.constantValue('val').withName('test1')
        .eq(rb.constantValue('val').withName('test2'))
        .withName('test');
    rb.addCondition(c1);
    expect(rb.getCondition(c1.name)).toBe(c1);
    var c2 = rb.constantValue('val').withName('test3')
        .eq(rb.constantValue('val').withName('test4'))
        .withName('test');
    expect(function () { return rb.addCondition(c2); }).toThrowError();
});
test("Can't add rule with same name as existing", function () {
    var rb = new RuleBase_1.default({
        store: makeStore()
    });
    var c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'));
    c1.name = 'c1';
    var c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'));
    c2.name = 'c2';
    var r1 = rb.when(c1).then(c2).withName('test');
    var c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'));
    c3.name = 'c3';
    var c4 = rb.constantValue('val').withName('test7').eq(rb.constantValue('val').withName('test8'));
    c4.name = 'c4';
    var r2 = new Rule_1.default({
        name: 'test',
        trigger: c3,
        action: c4
    });
    expect(function () { return rb.addRule(r2); }).toThrowError();
});
