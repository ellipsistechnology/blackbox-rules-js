"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TimerInterrupt = /** @class */ (function () {
    function TimerInterrupt(runPeriod) {
        this.runPeriod = runPeriod;
    }
    TimerInterrupt.prototype.start = function (rb, rule) {
        rule._interval = setInterval(function () { return rb.processRule(rule); }, this.runPeriod);
    };
    TimerInterrupt.prototype.stop = function (_rb, rule) {
        if (rule._interval)
            clearInterval(rule._interval);
    };
    return TimerInterrupt;
}());
exports.default = TimerInterrupt;
