import RuleInterrupt from "../RuleInterrupt";
import RuleBase, { Rule } from "../..";
export default class TimerInterrupt implements RuleInterrupt {
    runPeriod: number;
    constructor(runPeriod: number);
    start(rb: RuleBase, rule: Rule): void;
    stop(_rb: RuleBase, rule: Rule): void;
}
