import Condition from './Condition';
import Rule from './Rule';
import Value from './Value';
export default interface Store {
    getValue(name: string): Value<any>;
    putValue(value: Value<any>): any;
    removeValue(name: string): any;
    allValues(): Value<any>[];
    getCondition(name: string): Condition;
    putCondition(condition: Condition): any;
    removeCondition(name: string): any;
    allConditions(): Condition[];
    getRule(name: string): Rule;
    putRule(rule: Rule): any;
    removeRule(name: string): any;
    allRules(): Rule[];
}
