"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ConditionImpl = /** @class */ (function () {
    function ConditionImpl(_a) {
        var _b = _a === void 0 ? {} : _a, left = _b.left, right = _b.right, name = _b.name, type = _b.type, template = _b.template;
        this.left = left;
        this.right = right;
        this.name = name;
        this.type = type;
        this.template = template;
    }
    // TODO try and move and and or into their own classes
    ConditionImpl.prototype.and = function (cond) {
        // let c = new ConditionImpl({ left: this, right: cond, type: "and" })
        // c.checkTrue = async () => await c.left.get() && await c.right.get()
        // c.makeTrue = async () => {
        //   c.left.set(true);
        //   c.right.set(true);
        // }
        // return c
        return new AndCondition({ left: this, right: cond });
    };
    ConditionImpl.prototype.or = function (cond) {
        // let c = new ConditionImpl({ left: this, right: cond, type: "or" })
        // c.checkTrue = async () => await c.left.get() || await c.right.get()
        // c.makeTrue = async () => {}
        // return c
        return new OrCondition({ left: this, right: cond });
    };
    ConditionImpl.prototype.checkTrue = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                throw new Error('Condition.checkTrue() must be implemented in subclass');
            });
        });
    };
    ConditionImpl.prototype.makeTrue = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                throw new Error('Condition.makeTrue() must be implemented in subclass');
            });
        });
    };
    ConditionImpl.prototype.get = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.checkTrue()];
            });
        });
    };
    ConditionImpl.prototype.set = function (v) {
        if (v)
            this.makeTrue().then();
    };
    return ConditionImpl;
}());
exports.default = ConditionImpl;
var AndCondition = /** @class */ (function (_super) {
    __extends(AndCondition, _super);
    function AndCondition(props) {
        var _this = _super.call(this, props) || this;
        _this.type = 'and';
        return _this;
    }
    AndCondition.prototype.checkTrue = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.left.get()];
                    case 1:
                        _a = (_b.sent());
                        if (!_a) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.right.get()];
                    case 2:
                        _a = (_b.sent());
                        _b.label = 3;
                    case 3: return [2 /*return*/, _a];
                }
            });
        });
    };
    AndCondition.prototype.makeTrue = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.left.set(true);
                this.right.set(true);
                return [2 /*return*/];
            });
        });
    };
    return AndCondition;
}(ConditionImpl));
exports.AndCondition = AndCondition;
var OrCondition = /** @class */ (function (_super) {
    __extends(OrCondition, _super);
    function OrCondition(props) {
        var _this = _super.call(this, props) || this;
        _this.type = 'or';
        return _this;
    }
    OrCondition.prototype.checkTrue = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.left.get()];
                    case 1:
                        _a = (_b.sent());
                        if (_a) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.right.get()];
                    case 2:
                        _a = (_b.sent());
                        _b.label = 3;
                    case 3: return [2 /*return*/, _a];
                }
            });
        });
    };
    OrCondition.prototype.makeTrue = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    return OrCondition;
}(ConditionImpl));
exports.OrCondition = OrCondition;
