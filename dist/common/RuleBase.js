"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Rule_1 = require("./Rule");
var ConditionImpl_1 = require("./ConditionImpl");
var ConstantValue_1 = require("./values/ConstantValue");
var RemoteValue_1 = require("./values/RemoteValue");
var VariableValue_1 = require("./values/VariableValue");
var DateTimeValue_1 = require("./values/DateTimeValue");
var LogValue_1 = require("./values/LogValue");
var named_1 = require("./named");
var TimerInterrupt_1 = require("./interrupts/TimerInterrupt");
var RuleBuilder = /** @class */ (function () {
    function RuleBuilder(ruleBase, trigger) {
        if (!trigger.left || !trigger.right) { // if invalid then load by name
            this.trigger = ruleBase.getCondition(trigger.name);
            if (!this.trigger)
                throw "Trigger with name \"" + trigger.name + "\" could not be found.";
        }
        else {
            this.trigger = new ConditionImpl_1.default(trigger);
        }
        this.ruleBase = ruleBase;
        this.then = this.then.bind(this);
        this.makeRule = this.makeRule.bind(this);
    }
    RuleBuilder.prototype.then = function (action) {
        var _this = this;
        var r;
        if (!action.left || !action.right) {
            var readAction = this.ruleBase.getCondition(action.name);
            if (!readAction)
                throw new Error("Action with name \"" + action.name + "\" could not be found.");
            r = this.makeRule(readAction);
        }
        else {
            r = this.makeRule(new ConditionImpl_1.default(action));
        }
        return named_1.withName(r, function (r) { return _this.ruleBase.addRule(r); });
    };
    RuleBuilder.prototype.makeRule = function (action) {
        return new Rule_1.default({
            trigger: this.trigger,
            action: action
        });
    };
    return RuleBuilder;
}());
var RuleBase = /** @class */ (function () {
    function RuleBase(_a) {
        var _this = this;
        var _b = _a === void 0 ? {} : _a, store = _b.store, remoteValueProtocol = _b.remoteValueProtocol, variableStore = _b.variableStore, runPeriod = _b.runPeriod;
        this.store = store;
        this.remoteValueProtocol = remoteValueProtocol;
        this.variableStore = variableStore;
        this.runPeriod = runPeriod || runPeriod === 0 ? runPeriod : 1000;
        this.locks = {};
        this.uncond = new ConditionImpl_1.default({ type: "unconditional" });
        this.uncond.checkTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, true];
        }); }); };
        this.uncond.makeTrue = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); }); };
        this.processRules = this.processRules.bind(this);
    }
    RuleBase.prototype.getValue = function (valueName) {
        return this.store.getValue(valueName);
    };
    RuleBase.prototype.getCondition = function (conditionName) {
        return this.store.getCondition(conditionName);
    };
    RuleBase.prototype.getRule = function (ruleName) {
        return this.store.getRule(ruleName);
    };
    RuleBase.prototype.unconditional = function () {
        return this.uncond;
    };
    RuleBase.prototype.addValue = function (v) {
        if (!v)
            throw new Error('Cannot add null or undefined value.');
        if (this.store.getValue(v.name))
            throw new Error("Value already exists with name " + v.name);
        if (!v.name)
            throw new Error("Cannot add a value without a name: " + JSON.stringify(v));
        this.store.putValue(v);
    };
    RuleBase.prototype.removeValue = function (name) {
        this.store.removeValue(name);
    };
    RuleBase.prototype.loadOrAddValues = function (c) {
        var anyC = c;
        if (anyC.left) {
            if (anyC.left) {
                if (!anyC.left.type)
                    anyC.left = this.getValue(anyC.left.name);
                else if (!this.getValue(anyC.left.name))
                    this.addValue(anyC.left);
            }
            if (anyC.right) {
                if (!anyC.right.type)
                    anyC.right = this.getValue(anyC.right.name);
                else if (!this.getValue(anyC.right.name))
                    this.addValue(anyC.right);
            }
        }
    };
    RuleBase.prototype.addCondition = function (c) {
        if (!c)
            throw new Error('Cannot add null or undefined condition.');
        if (this.store.getCondition(c.name))
            throw new Error("Condition already exists with name " + c.name);
        if (!c.name)
            throw new Error("Cannot add a condition without a name: " + JSON.stringify(c));
        this.store.putCondition(c);
        // Load or add values:
        this.loadOrAddValues(c);
    };
    RuleBase.prototype.removeCondition = function (name) {
        this.store.removeCondition(name);
    };
    RuleBase.prototype.addConditions = function (r) {
        if (!this.getCondition(r.trigger.name))
            this.addCondition(r.trigger);
        if (!this.getCondition(r.action.name))
            this.addCondition(r.action);
        if (r.preCondition && !this.getCondition(r.preCondition.name))
            this.addCondition(r.preCondition);
        if (r.postCondition && !this.getCondition(r.postCondition.name))
            this.addCondition(r.postCondition);
    };
    RuleBase.prototype.addRule = function (r) {
        if (!r)
            throw "Cannot add null rule.";
        if (!r.name || r.name.length == 0)
            throw "Cannot add rule with null or empty name: \"" + r.name + "\"";
        if (this.store.getRule(r.name))
            throw new Error("Rule already exists with name " + r.name);
        if (!r.action)
            throw "Cannot add rule with null action.";
        if (!r.trigger)
            throw "Cannot add rule with null trigger.";
        if (!r.action.name || r.action.name.length == 0)
            throw "Cannot add rule with null or empty action name";
        if (!r.trigger.name || r.trigger.name.length == 0)
            throw "Cannot add rule with null or empty trigger name";
        if (r.preCondition && (!r.preCondition.name || r.preCondition.name.length == 0))
            throw "Cannot add rule with null or empty preCondition name";
        if (r.postCondition && (!r.postCondition.name || r.postCondition.name.length == 0))
            throw "Cannot add rule with null or empty postCondition name";
        this.addConditions(r);
        this.store.putRule(r);
    };
    RuleBase.prototype.removeRule = function (name) {
        this.store.removeRule(name);
    };
    RuleBase.prototype.replaceValue = function (v) {
        this.store.putValue(v);
    };
    RuleBase.prototype.replaceCondition = function (c) {
        this.loadOrAddValues(c);
        this.store.putCondition(c);
    };
    RuleBase.prototype.replaceRule = function (r) {
        this.addConditions(r);
        this.store.putRule(r);
    };
    RuleBase.prototype.processRule = function (rule) {
        return __awaiter(this, void 0, void 0, function () {
            var triggered;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.locks[rule.name])
                            return [2 /*return*/];
                        this.locks[rule.name] = true;
                        if (!rule.preCondition) return [3 /*break*/, 2];
                        return [4 /*yield*/, rule.preCondition.makeTrue()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, rule.trigger.checkTrue()];
                    case 3:
                        triggered = _a.sent();
                        if (!triggered) return [3 /*break*/, 5];
                        return [4 /*yield*/, rule.action.makeTrue()];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        if (!rule.postCondition) return [3 /*break*/, 7];
                        return [4 /*yield*/, rule.postCondition.makeTrue()];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7:
                        this.locks[rule.name] = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * TODO change this to be based on an interrupt field in the rule:
     *      E.g. a delay interrupt will execute the rule after a delay and then when
     *      finished will schedule the next execution and so on.
     *      This means that you simply start/init the rulebase and it will then schedule
     *      all interrupts. The user will need to be able to define their own interrupts also.
     *      Start/init will then replace processRules().
     * @return true if the rules were executed, false otherwise.
     */
    RuleBase.prototype.processRules = function () {
        return __awaiter(this, void 0, void 0, function () {
            var now;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        now = new Date();
                        if (this.lastRun && (now.getTime() - this.lastRun.getTime() < this.runPeriod))
                            return [2 /*return*/, false];
                        else
                            this.lastRun = now;
                        return [4 /*yield*/, Promise.all(this.store.allRules().map(function (rule) { return _this.processRule(rule); }))];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    RuleBase.prototype.start = function () {
        var _this = this;
        this.store.allRules().forEach(function (rule) {
            if (!rule.interrupt)
                rule.interrupt = new TimerInterrupt_1.default(_this.runPeriod);
            rule.interrupt.start(_this, rule);
        });
    };
    RuleBase.prototype.stop = function () {
        var _this = this;
        this.store.allRules().forEach(function (rule) {
            if (rule.interrupt)
                rule.interrupt.stop(_this, rule);
        });
    };
    RuleBase.prototype.when = function (trigger) {
        return new RuleBuilder(this, trigger);
    };
    RuleBase.prototype.allRules = function () {
        return this.store.allRules();
    };
    RuleBase.prototype.allConditions = function () {
        return this.store.allConditions();
    };
    RuleBase.prototype.allValues = function () {
        return this.store.allValues();
    };
    /////////////////////
    // Factory methods //
    /////////////////////
    RuleBase.prototype.constantValue = function (v) {
        var _this = this;
        var value = new ConstantValue_1.default(v);
        return named_1.withName(value, function (v) { return _this.addValue(v); });
    };
    RuleBase.prototype.remoteValue = function (remoteAddress, remoteValueName) {
        var _this = this;
        if (!this.remoteValueProtocol)
            throw "A RemoteValueProtocol must be assigned to the RuleBase prior to creating a RemoteValue object.";
        var value = new RemoteValue_1.default(this.remoteValueProtocol);
        value.remoteValueName = remoteValueName;
        value.remoteAddress = remoteAddress;
        return named_1.withName(value, function (v) { return _this.addValue(v); });
    };
    RuleBase.prototype.variableValue = function (variableName) {
        var _this = this;
        if (variableName === void 0) { variableName = null; }
        if (!this.variableStore)
            throw "A VariableStore must be assigned to the RuleBase prior to creating a VariableValue object.";
        if (variableName === null)
            variableName = name;
        var value = new VariableValue_1.default(this.variableStore);
        value.variableName = variableName;
        return named_1.withName(value, function (v) { return _this.addValue(v); });
    };
    RuleBase.prototype.dateTimeValue = function () {
        var _this = this;
        var value = new DateTimeValue_1.default();
        return named_1.withName(value, function (v) { return _this.addValue(v); });
    };
    RuleBase.prototype.logValue = function () {
        var _this = this;
        var value = new LogValue_1.default();
        return named_1.withName(value, function (v) { return _this.addValue(v); });
    };
    return RuleBase;
}());
exports.default = RuleBase;
