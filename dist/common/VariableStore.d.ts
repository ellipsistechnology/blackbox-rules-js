export default interface VariableStore {
    put(name: string, v: any): void;
    get(name: string): any;
}
