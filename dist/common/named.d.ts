export interface WithName<T> {
    withName(name: string): T;
}
export interface Named {
    name: string;
}
export declare function withName<T extends Named>(t: T, callback?: (t: T) => void): T & WithName<T>;
