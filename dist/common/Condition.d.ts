import Value from './Value';
export default interface Condition extends Value<boolean> {
    name: string;
    type: string;
    checkTrue(): Promise<boolean>;
    makeTrue(): Promise<void>;
}
