import RuleBase, { Rule } from "..";
export default interface RuleInterrupt {
    start(rb: RuleBase, rule: Rule): any;
    stop(rb: RuleBase, rule: Rule): any;
}
