"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function withName(t, callback) {
    if (callback === void 0) { callback = undefined; }
    return Object.assign(t, {
        withName: function (name) {
            t.name = name;
            if (callback)
                callback(t);
            return t;
        }
    });
}
exports.withName = withName;
