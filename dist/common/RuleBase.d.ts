import Rule from './Rule';
import Condition from './Condition';
import { ConditionData } from './ConditionImpl';
import Value from './Value';
import ConstantValue from './values/ConstantValue';
import RemoteValue from './values/RemoteValue';
import VariableValue from './values/VariableValue';
import DateTimeValue from './values/DateTimeValue';
import LogValue from './values/LogValue';
import Store from './Store';
import RemoteValueProtocol from './RemoteValueProtocol';
import VariableStore from './VariableStore';
import { WithName } from './named';
interface RuleBaseConfig {
    store?: Store;
    remoteValueProtocol?: RemoteValueProtocol;
    variableStore?: VariableStore;
    runPeriod?: number;
}
declare class RuleBuilder {
    ruleBase: RuleBase;
    trigger: Condition;
    constructor(ruleBase: RuleBase, trigger: ConditionData);
    then(action: ConditionData): Rule & WithName<Rule>;
    makeRule(action: Condition): Rule;
}
export default class RuleBase {
    store: Store;
    uncond: Condition;
    runPeriod: number;
    lastRun: Date;
    remoteValueProtocol: RemoteValueProtocol;
    variableStore: VariableStore;
    locks: {
        [key: string]: boolean;
    };
    constructor({ store, remoteValueProtocol, variableStore, runPeriod }?: RuleBaseConfig);
    getValue(valueName: string): Value<any>;
    getCondition(conditionName: string): Condition;
    getRule(ruleName: string): Rule;
    unconditional(): Condition;
    addValue(v: Value<any>): void;
    removeValue(name: string): void;
    private loadOrAddValues;
    addCondition(c: Condition): void;
    removeCondition(name: string): void;
    private addConditions;
    addRule(r: Rule): void;
    removeRule(name: string): void;
    replaceValue(v: Value<any>): void;
    replaceCondition(c: Condition): void;
    replaceRule(r: Rule): void;
    processRule(rule: Rule): Promise<void>;
    /**
     * TODO change this to be based on an interrupt field in the rule:
     *      E.g. a delay interrupt will execute the rule after a delay and then when
     *      finished will schedule the next execution and so on.
     *      This means that you simply start/init the rulebase and it will then schedule
     *      all interrupts. The user will need to be able to define their own interrupts also.
     *      Start/init will then replace processRules().
     * @return true if the rules were executed, false otherwise.
     */
    processRules(): Promise<boolean>;
    start(): void;
    stop(): void;
    when(trigger: ConditionData): RuleBuilder;
    allRules(): Rule[];
    allConditions(): Condition[];
    allValues(): Value<any>[];
    constantValue<T>(v: T): ConstantValue<T> & WithName<ConstantValue<T>>;
    remoteValue<T>(remoteAddress: string, remoteValueName: string): RemoteValue<T> & WithName<RemoteValue<T>>;
    variableValue<T>(variableName?: string): VariableValue<T> & WithName<VariableValue<T>>;
    dateTimeValue(): DateTimeValue & WithName<DateTimeValue>;
    logValue(): LogValue & WithName<LogValue>;
}
export {};
