import Condition from './Condition';
import Value from './Value';
import { WithName } from './named';
export interface ValueData {
    name?: string;
    type?: string;
}
export default class ValueImpl<T> implements Value<T> {
    name: string;
    type: string;
    constructor({ name, type }?: ValueData);
    eq(rhs: Value<T>): Condition & WithName<Condition>;
    lt(rhs: Value<T>): Condition & WithName<Condition>;
    gt(rhs: Value<T>): Condition & WithName<Condition>;
    /**
     * Gets the value of this Value object.
   * @return the current value
     */
    get(): Promise<T>;
    /**
     * Applies the given value to this Value object.
     * @param v The value to apply
     */
    set(_v: T): void;
}
