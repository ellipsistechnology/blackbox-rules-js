import Value from '../Value';
import VariableStore from '../VariableStore';
export declare const VARIABLE_TYPE = "variable";
export default class VariableValue<T> implements Value<T> {
    variableStore: VariableStore;
    variableName: string;
    name: string;
    type: string;
    constructor(store: VariableStore);
    set(v: T): void;
    get(): Promise<T>;
}
