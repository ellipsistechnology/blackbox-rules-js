import Value from '../Value';
import ValueImpl from '../ValueImpl';
export declare const CONSTANT_TYPE = "constant";
export default class ConstantValue<T> extends ValueImpl<T> implements Value<T> {
    type: string;
    val: any;
    name: string;
    constructor(value: T);
    get(): Promise<T>;
    set(value: T): void;
}
