import Value from '../Value';
import RemoteValueProtocol from '../RemoteValueProtocol';
export declare const REMOTE_TYPE = "remote";
export default class RemoteValue<T> implements Value<T> {
    remoteValueAddress: string;
    remoteAddress: string;
    remoteValueName: string;
    remoteValueProtocol: RemoteValueProtocol;
    name: string;
    type: string;
    constructor(remoteValueProtocol: RemoteValueProtocol);
    getRemoteValueAddress(): string;
    get(): Promise<T>;
    set(v: T): void;
}
