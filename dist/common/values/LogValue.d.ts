import Value from '../Value';
export declare const LOG_TYPE = "log";
export default class LogValue implements Value<string> {
    name: string;
    type: string;
    constructor();
    get(): Promise<string>;
    set(v: string): void;
}
