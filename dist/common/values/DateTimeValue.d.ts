import Value from '../Value';
export declare const DATE_TIME_TYPE = "date-time";
export default class DateTimeValue implements Value<Date> {
    name: string;
    type: string;
    constructor();
    get(): Promise<Date>;
    set(v: Date): void;
}
