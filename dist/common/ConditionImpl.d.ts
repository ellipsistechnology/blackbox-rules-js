import Condition from './Condition';
import Value from './Value';
/**
 * Condition types:
 *
 * Equals:          Left value must be equal to right value.
 * Less than:       Left value must be less than right value (trigger conditions only).
 * Greater than:    Left value must be greater than right value (trigger conditions only).
 * And:             Left value and right value are conditions both of which
 *                  must true for a trigger, both of which will be enforced if an action.
 * Or:              Left value and right value are conditions either of which
 *                  must be true (trigger conditions only).
 */
export interface ConditionData {
    name?: string;
    left?: Value<any>;
    right?: Value<any>;
    type?: string;
    template?: any;
}
export default class ConditionImpl implements Condition, ConditionData {
    name: string;
    left: Value<any>;
    right: Value<any>;
    type: string;
    template: any;
    constructor({ left, right, name, type, template }?: ConditionData);
    and(cond: Condition): Condition;
    or(cond: Condition): Condition;
    checkTrue(): Promise<boolean>;
    makeTrue(): Promise<void>;
    get(): Promise<boolean>;
    set(v: boolean): void;
}
export declare class AndCondition extends ConditionImpl {
    constructor(props: any);
    checkTrue(): Promise<boolean>;
    makeTrue(): Promise<void>;
}
export declare class OrCondition extends ConditionImpl {
    constructor(props: any);
    checkTrue(): Promise<any>;
    makeTrue(): Promise<void>;
}
