"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rule = /** @class */ (function () {
    function Rule(_a) {
        var _b = _a === void 0 ? {} : _a, name = _b.name, preCondition = _b.preCondition, postCondition = _b.postCondition, trigger = _b.trigger, action = _b.action, interrupt = _b.interrupt;
        this.name = name;
        this.preCondition = preCondition;
        this.postCondition = postCondition;
        this.trigger = trigger;
        this.action = action;
        this.interrupt = interrupt;
    }
    return Rule;
}());
exports.default = Rule;
