import ConditionImpl from '../ConditionImpl';
export declare const EQUALS_TYPE = "=";
export default class EqualsCondition extends ConditionImpl {
    constructor(props: any);
    checkTrue(): Promise<boolean>;
    makeTrue(): Promise<void>;
}
