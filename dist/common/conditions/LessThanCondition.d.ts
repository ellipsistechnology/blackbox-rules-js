import ConditionImpl from '../ConditionImpl';
export declare const LESS_THAN_TYPE = "<";
export default class LessThanCondition extends ConditionImpl {
    constructor(props: any);
    checkTrue(): Promise<boolean>;
    /**
     * Does nothing.
     */
    makeTrue(): Promise<void>;
}
