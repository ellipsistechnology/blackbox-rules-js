import ConditionImpl from '../ConditionImpl';
export declare const GREATER_THAN_TYPE = ">";
export default class GreaterThanCondition extends ConditionImpl {
    constructor(props: any);
    checkTrue(): Promise<boolean>;
    /**
     * Does nothing.
     */
    makeTrue(): Promise<void>;
}
