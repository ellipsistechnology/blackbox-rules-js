import Condition from './Condition';
import RuleInterrupt from './RuleInterrupt';
export interface RuleData {
    name?: string;
    preCondition?: Condition;
    postCondition?: Condition;
    trigger?: Condition;
    action?: Condition;
    interrupt?: RuleInterrupt;
}
export default class Rule implements RuleData {
    name: string;
    preCondition: Condition;
    postCondition: Condition;
    trigger: Condition;
    action: Condition;
    interrupt: RuleInterrupt;
    constructor({ name, preCondition, postCondition, trigger, action, interrupt }?: RuleData);
}
