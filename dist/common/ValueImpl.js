"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LessThanCondition_1 = require("./conditions/LessThanCondition");
var GreaterThanCondition_1 = require("./conditions/GreaterThanCondition");
var EqualsCondition_1 = require("./conditions/EqualsCondition");
var named_1 = require("./named");
var ValueImpl = /** @class */ (function () {
    function ValueImpl(_a) {
        var _b = _a === void 0 ? {} : _a, name = _b.name, type = _b.type;
        this.name = name;
        this.type = type;
    }
    ValueImpl.prototype.eq = function (rhs) {
        return named_1.withName(new EqualsCondition_1.default({ left: this, right: rhs }));
    };
    ValueImpl.prototype.lt = function (rhs) {
        return named_1.withName(new LessThanCondition_1.default({ left: this, right: rhs }));
    };
    ValueImpl.prototype.gt = function (rhs) {
        return named_1.withName(new GreaterThanCondition_1.default({ left: this, right: rhs }));
    };
    /**
     * Gets the value of this Value object.
   * @return the current value
     */
    ValueImpl.prototype.get = function () {
        throw "get() must be implemented in subclass";
    };
    /**
     * Applies the given value to this Value object.
     * @param v The value to apply
     */
    ValueImpl.prototype.set = function (_v) {
        throw "set(v) must be implemented in subclass";
    };
    return ValueImpl;
}());
exports.default = ValueImpl;
