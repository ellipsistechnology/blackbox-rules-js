import ValueImpl from '../common/ValueImpl'
import Value from '../common/Value'
import ConstantValue, {CONSTANT_TYPE} from '../common/values/ConstantValue'
import DateTimeValue, {DATE_TIME_TYPE} from '../common/values/DateTimeValue'
import RemoteValue, {REMOTE_TYPE} from '../common/values/RemoteValue'
import VariableValue, {VARIABLE_TYPE} from '../common/values/VariableValue'

import {testCondProps} from './Condition.test';

export const makeRemoteValueProtocol = (remoteValues) => {
  return {
    lookupAgentValueAddress: (agentAddress) => {
      return agentAddress+"/value"
    },
    setValue: async (agentValueAddress:string, remoteValueName:string, value:any) => {
      const agentAddress = agentValueAddress.split('/')[0]
      remoteValues[agentAddress][remoteValueName] = value
      return null
    },
    getValue: async (agentValueAddress:string, remoteValueName:string) => {
      const agentAddress = agentValueAddress.split('/')[0]
      return remoteValues[agentAddress][remoteValueName]
    }
  }
}

test('Value factory methods create valid conditions.', async () => {
  // Setup:
  const v1 = new ValueImpl<any>()
  const v2 = new ValueImpl<any>()

  v1.get = async () => 1
  v2.get = async () => 1

  // Test when equal:
  const eqCond = v1.eq(v2);
  const ltCond = v1.lt(v2)
  const gtCond = v1.gt(v2)

  testCondProps(eqCond)
  testCondProps(ltCond)
  testCondProps(gtCond)

  expect(await eqCond.checkTrue()).toBe(true)
  expect(await ltCond.checkTrue()).toBe(false)
  expect(await gtCond.checkTrue()).toBe(false)

  v2.get = async () => 2

  // Test when v1 < v2:
  const eqCond2 = v1.eq(v2)
  const ltCond2 = v1.lt(v2)
  const gtCond2 = v1.gt(v2)

  expect(await eqCond2.checkTrue()).toBe(false)
  expect(await ltCond2.checkTrue()).toBe(true)
  expect(await gtCond2.checkTrue()).toBe(false)

  // Test when v1 > v2:
  v2.get = async () => 0

  const eqCond3 = v1.eq(v2)
  const ltCond3 = v1.lt(v2)
  const gtCond3 = v1.gt(v2)

  expect(await eqCond3.checkTrue()).toBe(false)
  expect(await ltCond3.checkTrue()).toBe(false)
  expect(await gtCond3.checkTrue()).toBe(true)
})

test('ConstantValue', async () => {
  const constValue = new ConstantValue<any>(undefined)
  expect(constValue.type).toBe(CONSTANT_TYPE)
  expect(await constValue.get()).toBeUndefined()
  const v = 123.456
  constValue.set(v)
  expect(await constValue.get()).toBe(v)
})

test('DateTimeValue', async () => {
  const dateTimeValue = new DateTimeValue()
  expect(dateTimeValue.type).toBe(DATE_TIME_TYPE)
  expect(await dateTimeValue.get()).toBeInstanceOf(Date)
  expect(new Date().getTime() - (await dateTimeValue.get()).getTime()).toBeLessThan(1) // 1 millisecond precision
})

export const remoteValues = {
  agent1: {
    value1: 1,
    value2: 2
  },
  agent2: {
    value1: 3
  }
}

export const testRemoteValue = async (createRemoteValue) => {
  const agent1Value1Address = "agent1/value/value1"
  const agent1Value2Address = "agent1/value/value2"
  const agent2Value1Address = "agent2/value/value1"

  const remoteValue11 = createRemoteValue(agent1Value1Address, "value1")
  const remoteValue12 = createRemoteValue(agent1Value2Address, "value2")
  const remoteValue21 = createRemoteValue(agent2Value1Address, "value1")

  expect(remoteValue11.type).toBe(REMOTE_TYPE)
  expect(remoteValue12.type).toBe(REMOTE_TYPE)
  expect(remoteValue21.type).toBe(REMOTE_TYPE)

  expect(await remoteValue11.get()).toBe(remoteValues.agent1.value1)
  expect(await remoteValue12.get()).toBe(remoteValues.agent1.value2)
  expect(await remoteValue21.get()).toBe(remoteValues.agent2.value1)

  const v11 = 123.456
  const v12 = -11
  const v21 = 12

  remoteValue11.set(v11)
  remoteValue12.set(v12)
  remoteValue21.set(v21)

  expect(remoteValues.agent1.value1).toBe(v11)
  expect(remoteValues.agent1.value2).toBe(v12)
  expect(remoteValues.agent2.value1).toBe(v21)

  expect(await remoteValue11.get()).toBe(v11)
  expect(await remoteValue12.get()).toBe(v12)
  expect(await remoteValue21.get()).toBe(v21)
}

test('RemoteValue', async () => {
  const remoteValueProtocol = makeRemoteValueProtocol(remoteValues)

  await testRemoteValue((agentValueAddress:string, remoteValueName:string) => {
    const rv =  new RemoteValue<any>(remoteValueProtocol)
    rv.remoteValueAddress = agentValueAddress
    rv.remoteValueName = remoteValueName
    return rv
  })
})

export const makeVariableStore = () => {
  const vars = {}
  return {
    get: (variableName) => vars[variableName],
    put: (variableName, v) => vars[variableName] = v
  }
}

export const testVariableValue = async (makeVariableValue) => {
  const variableValue1 = makeVariableValue("v1")
  const variableValue2 = makeVariableValue("v2")
  const variableValue3 = makeVariableValue("v3")

  expect(variableValue1.type).toBe(VARIABLE_TYPE)
  expect(variableValue2.type).toBe(VARIABLE_TYPE)
  expect(variableValue3.type).toBe(VARIABLE_TYPE)

  expect(await variableValue1.get()).toBeUndefined()
  expect(await variableValue2.get()).toBeUndefined()
  expect(await variableValue3.get()).toBeUndefined()

  const v1 = 111
  const v2 = 222
  const v3 = 333

  variableValue1.set(v1)
  expect(await variableValue1.get()).toBe(v1)

  variableValue2.set(v2)
  expect(await variableValue1.get()).toBe(v1)
  expect(await variableValue2.get()).toBe(v2)

  variableValue3.set(v3)
  expect(await variableValue1.get()).toBe(v1)
  expect(await variableValue2.get()).toBe(v2)
  expect(await variableValue3.get()).toBe(v3)

  variableValue1.set(v2)
  variableValue2.set(v3)
  variableValue3.set(v1)
  expect(await variableValue1.get()).toBe(v2)
  expect(await variableValue2.get()).toBe(v3)
  expect(await variableValue3.get()).toBe(v1)
}

test('VariableValue', async () => {
  const variableStore = makeVariableStore()

  await testVariableValue((name:string) => {
    const vv = new VariableValue<any>(variableStore)
    vv.variableName = name
    return vv
  })
})
