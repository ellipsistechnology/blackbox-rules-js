import RuleBase from '../common/RuleBase'
import {
  makeRemoteValueProtocol,
  testRemoteValue,
  remoteValues,
  testVariableValue,
  makeVariableStore
} from './Value.test'
import Condition from '../common/Condition'
import ConditionImpl from '../common/ConditionImpl'
import Store from '../common/Store'
import Rule from '../common/Rule'
import Value from '../common/Value'
import { EqualsCondition, ValueImpl } from '..';

// TODO test locking of rule

function makeStore():Store {
  const conditions = {}
  const rules = {}
  const values = {}
  return {
    getCondition: (name:string) => conditions[name],
    putCondition: (cond:Condition) => {if(cond) conditions[cond.name] = cond},
    removeCondition: (name:string) => conditions[name] = undefined,
    conditions: conditions,
    allConditions: () => Object.values(conditions),

    getRule: (name:string) => rules[name],
    putRule: (rule:Rule) => {if(rule) rules[rule.name] = rule},
    removeRule: (name:string) => rules[name] = undefined,
    rules: rules,
    allRules: () => Object.values(rules),

    getValue: (name:string) => values[name],
    putValue: (value:Value<any>) => {if(value) values[value.name] = value},
    removeValue: (name:string) => values[name] = undefined,
    values: rules,
    allValues: () => Object.values(rules)
  } as Store;
}

test("Rule store can store, retrieve and delete conditions", () => {
  const rb = new RuleBase({
    store: makeStore()
  });

  const name = "test condition ~!@#$%^&*()`[]\\{}|:\";'<>?,./'"
  const c = new ConditionImpl();
  c.name = name

  rb.addCondition(c)
  expect((<any>rb.store).conditions[name]).toBeDefined()

  const readCond = rb.getCondition(name)
  expect(readCond).toBe(c)

  rb.removeCondition(name)
  expect((<any>rb.store).conditions[name]).toBeUndefined()

  const readCond2 = rb.getCondition(name)
  expect(readCond2).toBeUndefined()
})

test("Rule store can store, retrieve and delete rules", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const name = "test rule ~!@#$%^&*()`[]\\{}|:\";'<>?,./'"
  const r = new Rule({
    name: name,
    action: new ConditionImpl({name: "a"}),
    trigger: new ConditionImpl({name: "t"})
  })

  rb.addRule(r)
  expect((<any>rb.store).rules[name]).toBeDefined()

  const readRule = rb.getRule(name)
  expect(readRule).toBe(r)

  rb.removeRule(name)
  expect((<any>rb.store).rules[name]).toBeUndefined()

  const readRule2 = rb.getRule(name)
  expect(readRule2).toBeUndefined()
})

test("Rules can't be added without a valid name, trigger and action", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const r = {} as Rule;
  expect(() => rb.addRule(r)).toThrow()
  r.name = ""
  expect(() => rb.addRule(r)).toThrow()
  r.name = "test"
  expect(() => rb.addRule(r)).toThrow()
  r.trigger = {name: "t"} as Condition
  expect(() => rb.addRule(r)).toThrow()
  r.action = {name: "a"} as Condition
  rb.addRule(r) // Should add without error

  // Missing name:
  const r1 = new Rule({
    trigger: new ConditionImpl({name: "t"}),
    action: new ConditionImpl({name: "a"})
  })
  expect(() => rb.addRule(r1)).toThrow()

  // Invalid name:
  const r2 = new Rule({
    name: "",
    trigger: {name: "t"} as Condition,
    action: {name: "a"} as Condition
  })
  expect(() => rb.addRule(r2)).toThrow()

  // Missing action:
  const r3 = new Rule({
    name: "bob",
    trigger: {name: "t"} as Condition
  })
  expect(() => rb.addRule(r3)).toThrow()

  // Missing trigger:
  const r4 = new Rule({
    name: "bob",
    action: {name: "a"} as Condition
  })
  expect(() => rb.addRule(r4)).toThrow()

  // Missing trigger name:
  const r5 = new Rule({
    name: "bob",
    action: {name: "a"} as Condition,
    trigger: {} as Condition
  })
  expect(() => rb.addRule(r5)).toThrow()

  // Missing action name:
  const r6 = new Rule({
    name: "bob",
    action: {} as Condition,
    trigger: {name: "t"} as Condition
  })
  expect(() => rb.addRule(r6)).toThrow()
})

test("Rules can't be added with an invalid pre or post condition", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const r1 = {
    name: "test1",
    preCondition: {},
    trigger: { name: "trigger" } as Condition,
    action: { name: "action" } as Condition
  } as Rule;

  expect(() => rb.addRule(r1)).toThrow() // invalid preCondition
  r1.preCondition.name = "pre"
  rb.addRule(r1) // Should add without error

  const r2 = {
    name: "test2",
    preCondition: { name: "pre" } as Condition,
    trigger: { name: "trigger" } as Condition,
    action: { name: "action" } as Condition
  } as Rule;

  r2.postCondition = new ConditionImpl()
  expect(() => rb.addRule(r2)).toThrow() // invalid postCondtion
  r2.postCondition.name = "post"
  rb.addRule(r2) // Should add without error
})

test("Variable store can store, retrieve and delete", async () => {
  const rb = new RuleBase({
    variableStore: makeVariableStore(),
    store: makeStore()
  })

  await testVariableValue((name) => rb.variableValue(name))
})

test("Remote value protocol can store, retrieve and delete", async () => {
  const rb = new RuleBase({
    remoteValueProtocol: makeRemoteValueProtocol(remoteValues),
    store: makeStore()
  })

  let i = 0;
  await testRemoteValue((agentValueAddress, remote) => {
    return rb.remoteValue(agentValueAddress, remote).withName(`test${i++}`)
  })
})

test("Unconditional is unconditional",  async () => {
  const rb = new RuleBase({})
  const uncond = rb.unconditional()
  expect(await uncond.checkTrue()).toBeTruthy()
  await uncond.makeTrue()
  expect(await uncond.checkTrue()).toBeTruthy()
})

test("Rules can be created with when(trigger).then(action).withName(name)", () => {
  const rb = new RuleBase({ store: makeStore() });
  (<any>rb.store).conditions.trigger = {name:"trigger", type:'t1'};
  (<any>rb.store).conditions.action = {name:"action", type:'t2'};

  const name = "rule name"
  const r = rb.when({name:"trigger"}).then({name:"action"}).withName(name)
  expect(r).toBeDefined()
  expect(r.trigger).toBeDefined()
  expect(r.action).toBeDefined()
  expect(r.name).toBeDefined()
  expect(r.trigger.name).toBe("trigger")
  expect(r.action.name).toBe("action")
  expect(r.trigger.type).toBe("t1")
  expect(r.action.type).toBe("t2")
  expect(r.name).toBe(name)

  expect(rb.getRule(name)).toBe(r)
})

test("All factory methods can be used together", () => {
  const rb = new RuleBase({ store: makeStore() });

  const r = rb.when(
    rb.constantValue('val').withName('test1')
    .eq(rb.constantValue('val').withName('test2'))
    .withName('trigger')
  ).then(
    rb.constantValue('val').withName('test3')
    .eq(rb.constantValue('val').withName('test4'))
    .withName('action')
  ).withName('test')

  expect(rb.getRule('test')).toBe(r)
})

function makeCondition(
  {name, left, right, makeTrue, checkTrue}
  :{name?:string, left?:Value<any>, right?:Value<any>, makeTrue?:()=>Promise<void>, checkTrue?:()=>Promise<boolean>}
):Condition {
  const c = new ConditionImpl({left:left, right:right, name:name})
  if(makeTrue)
    c.makeTrue = makeTrue
  if(checkTrue)
    c.checkTrue = checkTrue
  return c
}

test("Rules are processed after run period with interrupts", async () => {
    const rb = new RuleBase({
      store: makeStore(),
      runPeriod: 100
    })
    expect(rb.runPeriod).toBe(100)

    let count = 0
    const calls:any = {}
    rb.addRule({
      name: "test",
      preCondition: makeCondition({
        name: "pre",
        makeTrue: async () => { calls.pre = count++ }
      }),
      trigger: makeCondition({
        name: "trigger",
        checkTrue: async () => true
      }),
      action: makeCondition({
        name: "action",
        makeTrue: async () => { calls.act = count++ }
      }),
      postCondition: makeCondition({
        name: "post",
        makeTrue: async () => { calls.post = count++ }
      })
    } as Rule)

    rb.start()

    await new Promise(resolve => setTimeout(resolve, 200))
    expect(calls.pre).toBeDefined();
    expect(calls.post).toBeDefined();
    expect(calls.pre).toBeLessThan(calls.post);
    expect(calls.act).toBeDefined();
    expect(calls.act).toBeGreaterThan(0)

    calls.pre = calls.post = calls.act = 0; // reset
    await new Promise(resolve => setTimeout(resolve, 200))
    expect(calls.pre).toBeLessThan(calls.post);
    expect(calls.act).toBeGreaterThan(0)

    rb.stop()
    calls.pre = calls.post = calls.act = 0; // reset
    await new Promise(resolve => setTimeout(resolve, 200))
    expect(calls.pre).toEqual(0);
    expect(calls.post).toEqual(0);
    expect(calls.act).toEqual(0);
  })

test("Rules are processed after run period", async () => {
  const rb = new RuleBase({
    store: makeStore(),
    runPeriod: 0
  })
  expect(rb.runPeriod).toBe(0)

  let count = 0
  const calls:any = {}
  const act = jest.fn();
  rb.addRule({
    name: "test",
    preCondition: makeCondition({
      name: "pre",
      makeTrue: async () => { calls.pre = count++ }
    }),
    trigger: makeCondition({
      name: "trigger",
      checkTrue: async () => true
    }),
    action: makeCondition({
      name: "action",
      makeTrue: act
    }),
    postCondition: makeCondition({
      name: "post",
      makeTrue: async () => { calls.post = count++ }
    })
  } as Rule)

  let processed = await rb.processRules()
  expect(processed).toBe(true);
  expect(calls.pre).toBeDefined();
  expect(calls.post).toBeDefined();
  expect(calls.pre).toBeLessThan(calls.post);
  expect(act).toHaveBeenCalledTimes(1);

  calls.pre = calls.post = 0; // reset
  (rb.store as any).rules.test.trigger.checkTrue = () => false;
  processed = await rb.processRules()
  expect(processed).toBe(true);
  expect(calls.pre).toBeLessThan(calls.post);
  expect(act).toHaveBeenCalledTimes(1); // shouldn't have been called again
})

test("Rules are processed only after runPeriod", async () => {
  const rb = new RuleBase({
    store: makeStore(),
    runPeriod: 1000
  })

  const trig = jest.fn()
  rb.addRule(new Rule({
    name: "test",
    trigger: makeCondition({name: "trigger", checkTrue: trig}),
    action: makeCondition({name: "action"})
  }))

  // Two quick executions shouldn't process twice:
  let run = await rb.processRules()
  expect(run).toBe(true)
  expect(trig).toHaveBeenCalledTimes(1)
  run = await rb.processRules()
  expect(run).toBe(false)
  expect(trig).toHaveBeenCalledTimes(1)

  // Simulate runPeriod passing by setting runPeriod to now-lastRun:
  rb.runPeriod = new Date().getTime() - rb.lastRun.getTime()
  run = await rb.processRules()
  expect(run).toBe(true)
  expect(trig).toHaveBeenCalledTimes(2)
})

test("Values created with rulebase factory methods are stored in the rulebase.", () => {
  const rb = new RuleBase({
    store: makeStore(),
    remoteValueProtocol: makeRemoteValueProtocol({}),
    variableStore: makeVariableStore()
  })

  const rv = rb.remoteValue('test', 'test').withName('test1')
  expect(rb.getValue('test1')).toBe(rv)

  const cv = rb.constantValue('val').withName('test2')
  expect(rb.getValue('test2')).toBe(cv)

  const dtv = rb.dateTimeValue().withName('test3')
  expect(rb.getValue('test3')).toBe(dtv)

  const vv1 = rb.variableValue('var name').withName('test4')
  expect(rb.getValue('test4')).toBe(vv1)

  const vv2 = rb.variableValue().withName('test5')
  expect(rb.getValue('test5')).toBe(vv2)

  const lv = rb.logValue().withName('test6')
  expect(rb.getValue('test6')).toBe(lv)
})

test("Adding a rule adds conditions and values", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const rule = new Rule({
    name: 'test rule',
    trigger: new EqualsCondition({
      name: 'trigger',
      left: {name:'trigger.left', type: 'test', get: () => null, set: () => {}},
      right: {name:'trigger.right', type: 'test', get: () => null, set: () => {}}
    }),
    action: new EqualsCondition({
      name: 'action',
      left: {name:'action.left', type: 'test', get: () => null, set: () => {}},
      right: {name:'action.right', type: 'test', get: () => null, set: () => {}}
    })
  })

  rb.addRule(rule)

  expect(rb.getValue((rule.trigger as ConditionImpl).left.name))
    .toBe((rule.trigger as ConditionImpl).left)
  expect(rb.getValue((rule.trigger as ConditionImpl).right.name))
    .toBe((rule.trigger as ConditionImpl).right)

  expect(rb.getValue((rule.action as ConditionImpl).left.name))
    .toBe((rule.action as ConditionImpl).left)
  expect(rb.getValue((rule.action as ConditionImpl).right.name))
    .toBe((rule.action as ConditionImpl).right)

  expect(rb.getCondition(rule.trigger.name)).toBe(rule.trigger)
  expect(rb.getCondition(rule.action.name)).toBe(rule.action)

  expect(rb.getRule(rule.name)).toBe(rule)
})

test("Can't create value with same name as existing", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  rb.constantValue('val').withName('test')
  expect(() => rb.constantValue('val').withName('test')).toThrowError()
})

test("Can't create value without a name", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  expect(() => rb.constantValue('val').withName('')).toThrowError()
  expect(() => rb.constantValue('val').withName(null)).toThrowError()
  expect(() => rb.constantValue('val').withName(undefined)).toThrowError()
})

test("Can't create condition without a name", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const c1 = rb.constantValue('val').withName('test').eq(rb.constantValue('val').withName('test2'))
  expect(() => rb.addCondition(c1)).toThrowError()

  const c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'))
  c2.name = ''
  expect(() => rb.addCondition(c2)).toThrowError()

  const c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'))
  c2.name = null
  expect(() => rb.addCondition(c3)).toThrowError()
})

test("Can replace value", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  rb.constantValue('val').withName('test')
  const v = new ValueImpl()
  v.name = 'test'
  v.type = 't1'
  rb.replaceValue(v)
  expect(rb.getValue('test')).toBe(v)
})

test("Can replace condition", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'))
  c1.name = 'test cond'
  rb.addCondition(c1)
  const c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'))
  c2.name = 'test cond'
  rb.replaceCondition(c2)
  expect(rb.getCondition('test cond')).toBe(c2)
  expect(rb.getValue('test3')).toBe(((c2 as any) as ConditionImpl).left)
  expect(rb.getValue('test4')).toBe(((c2 as any) as ConditionImpl).right)
})

test("Can replace rule", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'))
  c1.name = 'c1'
  const c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'))
  c2.name = 'c2'
  const r1 = rb.when(c1).then(c2).withName('test')

  const c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'))
  c3.name = 'c3'
  const c4 = rb.constantValue('val').withName('test7').eq(rb.constantValue('val').withName('test8'))
  c4.name = 'c4'
  const r2 = new Rule({
    name:'test',
    trigger: c3,
    action: c4
  })
  rb.replaceRule(r2)
  expect(rb.getRule('test')).toBe(r2)

  expect(rb.getCondition('c3')).toBe(c3)
  expect(rb.getCondition('c4')).toBe(c4)

  expect(rb.getValue('test5')).toBe(((c3 as any) as ConditionImpl).left)
  expect(rb.getValue('test6')).toBe(((c3 as any) as ConditionImpl).right)

  expect(rb.getValue('test7')).toBe(((c4 as any) as ConditionImpl).left)
  expect(rb.getValue('test8')).toBe(((c4 as any) as ConditionImpl).right)
})

test("Can't add condtion with same name as existing", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const c1 = rb.constantValue('val').withName('test1')
    .eq(rb.constantValue('val').withName('test2'))
    .withName('test')
  rb.addCondition(c1)
  expect(rb.getCondition(c1.name)).toBe(c1)

  const c2 = rb.constantValue('val').withName('test3')
    .eq(rb.constantValue('val').withName('test4'))
    .withName('test')
  expect(() => rb.addCondition(c2)).toThrowError()
})

test("Can't add rule with same name as existing", () => {
  const rb = new RuleBase({
    store: makeStore()
  })

  const c1 = rb.constantValue('val').withName('test1').eq(rb.constantValue('val').withName('test2'))
  c1.name = 'c1'
  const c2 = rb.constantValue('val').withName('test3').eq(rb.constantValue('val').withName('test4'))
  c2.name = 'c2'
  const r1 = rb.when(c1).then(c2).withName('test')

  const c3 = rb.constantValue('val').withName('test5').eq(rb.constantValue('val').withName('test6'))
  c3.name = 'c3'
  const c4 = rb.constantValue('val').withName('test7').eq(rb.constantValue('val').withName('test8'))
  c4.name = 'c4'
  const r2 = new Rule({
    name:'test',
    trigger: c3,
    action: c4
  })

  expect(() => rb.addRule(r2)).toThrowError()
})
