import Condition from '../common/Condition'
import ConditionImpl from '../common/ConditionImpl'
import EqualsCondition, {EQUALS_TYPE} from '../common/conditions/EqualsCondition'
import GreaterThanCondition, {GREATER_THAN_TYPE} from '../common/conditions/GreaterThanCondition'
import LessThanCondition, {LESS_THAN_TYPE} from '../common/conditions/LessThanCondition'

export function testCondProps(cond) {
  expect(cond.left).toBeDefined()
  expect(cond.right).toBeDefined()
  expect(cond.type).toBeDefined()
  expect(cond.checkTrue).toBeDefined()
  expect(cond.makeTrue).toBeDefined()
}

test("Condition factory methods create valid conditions.", async () => {
  let c1State = false
  const c1 = new ConditionImpl()
  c1.checkTrue = async () => c1State
  c1.makeTrue = async () => { c1State = true }

  let c2State = false
  const c2 = new ConditionImpl()
  c2.checkTrue = async () => c2State
  c2.makeTrue = async () => { c2State = true }

  const andCond = c1.and(c2)
  const orCond = c1.or(c2)

  expect(await andCond.checkTrue()).toBe(false)
  expect(await orCond.checkTrue()).toBe(false)

  await c1.makeTrue()
  expect(await andCond.checkTrue()).toBe(false)
  expect(await orCond.checkTrue()).toBe(true)

  await c2.makeTrue()
  expect(await andCond.checkTrue()).toBe(true)
  expect(await orCond.checkTrue()).toBe(true)
})

test("EqualsCondition", async () => {
  let v1State = 1
  const v1 = {
    get: async () =>  v1State,
    set: (v) => {
      if(v)
        v1State = v
    }
  }
  let v2State = 2
  const v2 = {
    get: async () => v2State,
    set: (v) => {
      if(v)
        v2State = v
    }
  }

  const c = new EqualsCondition({
    left: v1,
    right: v2
  })

  expect(await c.checkTrue()).toBe(false)
  await c.makeTrue()
  expect(v1State).toBe(2)
  expect(v2State).toBe(2)
  expect(await c.checkTrue()).toBe(true)
})

test("LessThanCondition", async () => {
  let v1State = 1
  const v1 = {
    get: (v) => v1State
  }

  let v2State = 2
  const v2 = {
    get: () => v2State
  }

  const c = new LessThanCondition({
    left: v1,
    right: v2
  })

  expect(await c.checkTrue()).toBe(true)
  v2State = 1
  expect(await c.checkTrue()).toBe(false)
})

test("GreaterThanCondition", async () => {
  let v1State = 1
  const v1 = {
    get: async () => v1State,
    set: (v) => {}
  }

  let v2State = 1
  const v2 = {
    get: async () => v2State,
    set: (v) => {}
  }

  const c = new GreaterThanCondition({
    left: v1,
    right: v2
  })

  expect(await c.checkTrue()).toBe(false)
  v2State = 0
  expect(await c.checkTrue()).toBe(true)
})
