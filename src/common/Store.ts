import Condition from './Condition'
import Rule from './Rule'
import Value from './Value'

export default interface Store {
  getValue(name:string):Value<any>
  putValue(value:Value<any>)
  removeValue(name:string)
  allValues():Value<any>[]

  getCondition(name:string):Condition
  putCondition(condition:Condition)
  removeCondition(name:string)
  allConditions():Condition[]

  getRule(name:string):Rule
  putRule(rule:Rule)
  removeRule(name:string)
  allRules():Rule[]
}
