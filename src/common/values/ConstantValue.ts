import Value from '../Value'
import ValueImpl from '../ValueImpl';

export const CONSTANT_TYPE = "constant"

export default class ConstantValue<T> extends ValueImpl<T> implements Value<T> {
  type:string
  val:any
  name:string

	constructor(value:T)
	{
    super()
		this.type = CONSTANT_TYPE
    this.val = value
	}

	async get():Promise<T>
	{
    return this.val
	}

	set(value:T)
	{
    if(value)
		  this.val = value
	}
}
