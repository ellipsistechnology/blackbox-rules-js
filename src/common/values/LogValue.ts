import Value from '../Value'

export const LOG_TYPE = "log"

export default class LogValue implements Value<string> {
	name:string
	type:string

  constructor()
  {
    this.type = LOG_TYPE
  }

	async get():Promise<string> {
		return undefined
	}

	set(v:string) {
		console.log(v)
	}
}
