import Value from '../Value'

export const DATE_TIME_TYPE = "date-time"

// TODO Date only? Time only?
export default class DateTimeValue implements Value<Date> {
  name:string
  type:string

  constructor()
  {
    this.type = DATE_TIME_TYPE
  }

  async get():Promise<Date>
	{
		return new Date()
	}

  set(v:Date)
	{
		// do nothing
	}
}
