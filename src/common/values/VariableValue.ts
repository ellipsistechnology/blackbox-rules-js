import Value from '../Value'
import VariableStore from '../VariableStore'

export const VARIABLE_TYPE = "variable"

export default class VariableValue<T> implements Value<T> {
	variableStore:VariableStore
	variableName:string
  name:string
  type:string

	constructor(store:VariableStore)
	{
		this.variableStore = store;
		this.type = VARIABLE_TYPE;
	}

	set(v:T)
	{
    if(v)
		  this.variableStore.put(this.variableName, v);
	}

	async get():Promise<T>
	{
		return this.variableStore.get(this.variableName) as T;
	}
}
