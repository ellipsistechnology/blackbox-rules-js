import Value from '../Value'
import RemoteValueProtocol from '../RemoteValueProtocol'

export const REMOTE_TYPE = "remote"

export default class RemoteValue<T> implements Value<T> {
  remoteValueAddress: string
  remoteAddress: string
  remoteValueName: string
  remoteValueProtocol: RemoteValueProtocol
  name:string
  type:string

	constructor(remoteValueProtocol:RemoteValueProtocol)
	{
		this.remoteValueProtocol = remoteValueProtocol;
		this.type = REMOTE_TYPE;

    this.getRemoteValueAddress = this.getRemoteValueAddress.bind(this)
    this.get = this.get.bind(this)
    this.set = this.set.bind(this)
	}

	getRemoteValueAddress():string
	{
		if(!this.remoteValueAddress)
		{
			this.remoteValueAddress = this.remoteValueProtocol.lookupAgentValueAddress(this.remoteAddress)
		}
		return this.remoteValueAddress;
	}

  async get():Promise<T> {
    const response = await this.remoteValueProtocol.getValue<T>(this.getRemoteValueAddress(), this.remoteValueName);
    return response;
  }

	set(v:T)
	{
    if(v) {
		  this.remoteValueProtocol.setValue(this.getRemoteValueAddress(), this.remoteValueName, v)
        .then( (response)  => {
          // TODO error checking and logging
          // console.log(`Sent remote value ${v} to ${this.getRemoteValueAddress()}/${this.remoteValue.name}; response: ${response}`);
        })
        .catch( (error:any) => {
          // TODO error checking and logging
          console.error(error);
        })
    }
	}
}
