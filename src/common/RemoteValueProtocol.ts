import Value from './Value'

export default interface RemoteValueProtocol {
  lookupAgentValueAddress(agentAddress:string): string
  setValue<T>(agentValueAddress:string, remoteValueName:string, value:any): Promise<T>
  getValue<T>(agentValueAddress:string, remoteValueName:string): Promise<T>
}
