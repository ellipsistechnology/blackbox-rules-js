import ConditionImpl from '../ConditionImpl'

export const LESS_THAN_TYPE = "<"

export default class LessThanCondition extends ConditionImpl
{
 constructor(props)
 {
   super(props)
   this.type = LESS_THAN_TYPE
 }

 async checkTrue():Promise<boolean>
 {
   const l = await this.left.get()
   const r = await this.right.get()
   return l < r
 }

 /**
  * Does nothing.
  */
 async makeTrue()
 {
   // N/A
 }
}
