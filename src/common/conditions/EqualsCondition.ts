import ConditionImpl from '../ConditionImpl'

export const EQUALS_TYPE = "="

export default class EqualsCondition extends ConditionImpl {

  constructor(props) {
   super(props)
   this.type = EQUALS_TYPE
 }

 async checkTrue():Promise<boolean> {
   const leftValue = await this.left.get()
   const rightValue = await this.right.get()
   return leftValue === rightValue
 }

 async makeTrue():Promise<void> {
   const val = await this.right.get();
   return this.left.set(val);
 }
}
