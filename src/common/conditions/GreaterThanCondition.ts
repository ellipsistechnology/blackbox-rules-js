import ConditionImpl from '../ConditionImpl'
import Value from '../Value'

export const GREATER_THAN_TYPE = ">"

export default class GreaterThanCondition extends ConditionImpl
{
 constructor(props)
 {
   super(props)
   this.type = GREATER_THAN_TYPE
 }

 async checkTrue():Promise<boolean>
 {
   const l = await this.left.get()
   const r = await this.right.get()
   return l > r
 }

 /**
  * Does nothing.
  */
 async makeTrue()
 {
   // N/A
 }
}
