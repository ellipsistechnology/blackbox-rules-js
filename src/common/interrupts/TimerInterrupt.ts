import RuleInterrupt from "../RuleInterrupt";
import RuleBase, { Rule } from "../..";

export default class TimerInterrupt implements RuleInterrupt {
  runPeriod:number

  constructor(runPeriod:number) {
    this.runPeriod = runPeriod
  }

  start(rb: RuleBase, rule: Rule) {
    (<any>rule)._interval = setInterval(() => rb.processRule(rule), this.runPeriod)
  }

  stop(_rb: RuleBase, rule: Rule) {
    if((<any>rule)._interval)
      clearInterval((<any>rule)._interval)
  }
}
