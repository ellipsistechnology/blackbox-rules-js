export interface WithName<T> {
  withName(name:string):T
}

export interface Named {
  name:string
}

export function withName<T extends Named>(t:T, callback:(t:T)=>void = undefined):T&WithName<T> {
  return Object.assign(t, {
    withName: (name:string) => {
      t.name = name
      if(callback)
        callback(t)
      return t
    }
  })
}
