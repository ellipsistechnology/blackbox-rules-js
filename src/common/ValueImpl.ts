import Condition from './Condition';
import Value from './Value'
import LessThanCondition from './conditions/LessThanCondition'
import GreaterThanCondition from './conditions/GreaterThanCondition'
import EqualsCondition from './conditions/EqualsCondition'
import {WithName, withName} from './named'

export interface ValueData {
  name?:string
	type?:string
}

export default class ValueImpl<T> implements Value<T> {
  name:string
	type:string

	constructor({name, type}:ValueData={}) {
    this.name = name
    this.type = type
  }

	eq(rhs:Value<T>):Condition&WithName<Condition> {
    return withName(new EqualsCondition({left:this, right:rhs}))
	}

	lt(rhs:Value<T>):Condition&WithName<Condition>
	{
		return withName(new LessThanCondition({left:this, right:rhs}))
	}

	gt(rhs:Value<T>):Condition&WithName<Condition>
	{
		return withName(new GreaterThanCondition({left:this, right:rhs}))
	}

	/**
	 * Gets the value of this Value object.
   * @return the current value
	 */
	get():Promise<T>
  {
    throw "get() must be implemented in subclass"
  }

	/**
	 * Applies the given value to this Value object.
	 * @param v The value to apply
	 */
	set(_v:T) {
		throw "set(v) must be implemented in subclass"
	}
}
