import Rule from './Rule'
import ConditionImpl from './ConditionImpl'
import Condition from './Condition'
import {ConditionData} from './ConditionImpl'
import Value from './Value'

import ConstantValue from './values/ConstantValue'
import RemoteValue from './values/RemoteValue'
import VariableValue from './values/VariableValue'
import DateTimeValue from './values/DateTimeValue'
import LogValue from './values/LogValue'

import Store from './Store'
import RemoteValueProtocol from './RemoteValueProtocol'
import VariableStore from './VariableStore'

import {WithName, withName} from './named'
import TimerInterrupt from './interrupts/TimerInterrupt';

interface RuleBaseConfig {
	store?:Store,
	remoteValueProtocol?:RemoteValueProtocol,
	variableStore?:VariableStore,
	runPeriod?:number
}

class RuleBuilder
{
	ruleBase:RuleBase
	trigger:Condition

	constructor(ruleBase:RuleBase, trigger:ConditionData)
	{
    if(!trigger.left || !trigger.right) { // if invalid then load by name
      this.trigger = ruleBase.getCondition(trigger.name)
      if(!this.trigger)
        throw `Trigger with name "${trigger.name}" could not be found.`
    }
    else {
		  this.trigger = new ConditionImpl(trigger)
    }

		this.ruleBase = ruleBase

    this.then = this.then.bind(this)
    this.makeRule = this.makeRule.bind(this)
	}

	then(action:ConditionData):Rule&WithName<Rule>
	{
		let r:Rule;
    if(!action.left || !action.right) {
      const readAction = this.ruleBase.getCondition(action.name)
      if(!readAction)
        throw new Error(`Action with name "${action.name}" could not be found.`)
      r = this.makeRule(readAction)
    }
    else {
		  r = this.makeRule(new ConditionImpl(action))
    }
		return withName(r, (r:Rule) => this.ruleBase.addRule(r))
	}

	makeRule(action:Condition):Rule
	{
		return new Rule({
      trigger: this.trigger,
			action: action
    })
	}
}

export default class RuleBase {
	store:Store
	uncond:Condition
	runPeriod:number
	lastRun:Date
	remoteValueProtocol:RemoteValueProtocol
	variableStore:VariableStore
	locks:{[key:string]:boolean}

	constructor({ store, remoteValueProtocol, variableStore, runPeriod }: RuleBaseConfig = {}) {
		this.store = store
    this.remoteValueProtocol = remoteValueProtocol
    this.variableStore = variableStore
    this.runPeriod = runPeriod || runPeriod === 0 ? runPeriod : 1000
		this.locks = {}

    this.uncond = new ConditionImpl({type: "unconditional"})
		this.uncond.checkTrue = async () => true
    this.uncond.makeTrue = async () => {}

		this.processRules = this.processRules.bind(this)
	}

	getValue(valueName:string):Value<any> {
		return this.store.getValue(valueName)
	}

	getCondition(conditionName:string):Condition {
		return this.store.getCondition(conditionName)
	}

	getRule(ruleName:string):Rule {
		return this.store.getRule(ruleName)
	}

	unconditional():Condition {
		return this.uncond
	}

	addValue(v:Value<any>) {
		if(!v)
			throw new Error('Cannot add null or undefined value.')
		if(this.store.getValue(v.name))
			throw new Error(`Value already exists with name ${v.name}`)
		if(!v.name)
			throw new Error(`Cannot add a value without a name: ${JSON.stringify(v)}`)
		this.store.putValue(v)
	}

	removeValue(name:string) {
		this.store.removeValue(name)
	}

	private loadOrAddValues(c:Condition) {
		const anyC = <any>c
		if(anyC.left) {
			if(anyC.left) {
				if(!anyC.left.type)
					anyC.left = this.getValue(anyC.left.name)
				else if(!this.getValue(anyC.left.name))
					this.addValue(anyC.left)
			}

			if(anyC.right) {
				if(!anyC.right.type)
					anyC.right = this.getValue(anyC.right.name)
				else if(!this.getValue(anyC.right.name))
					this.addValue(anyC.right)
			}
		}
	}

	addCondition(c:Condition) {
		if(!c)
			throw new Error('Cannot add null or undefined condition.')
		if(this.store.getCondition(c.name))
			throw new Error(`Condition already exists with name ${c.name}`)
		if(!c.name)
			throw new Error(`Cannot add a condition without a name: ${JSON.stringify(c)}`)
		this.store.putCondition(c);

		// Load or add values:
		this.loadOrAddValues(c)
	}

	removeCondition(name:string) {
		this.store.removeCondition(name)
	}

	private addConditions(r:Rule) {
		if(!this.getCondition(r.trigger.name))
			this.addCondition(r.trigger)

		if(!this.getCondition(r.action.name))
			this.addCondition(r.action)

    if(r.preCondition && !this.getCondition(r.preCondition.name))
			this.addCondition(r.preCondition)

		if(r.postCondition && !this.getCondition(r.postCondition.name))
			this.addCondition(r.postCondition)
	}

	addRule(r:Rule)
	{
		if(!r)
			throw "Cannot add null rule."
		if(!r.name || r.name.length == 0)
			throw `Cannot add rule with null or empty name: "${r.name}"`
		if(this.store.getRule(r.name))
			throw new Error(`Rule already exists with name ${r.name}`)
		if(!r.action)
			throw "Cannot add rule with null action."
		if(!r.trigger)
			throw "Cannot add rule with null trigger."
    if(!r.action.name || r.action.name.length == 0)
      throw "Cannot add rule with null or empty action name"
    if(!r.trigger.name || r.trigger.name.length == 0)
      throw "Cannot add rule with null or empty trigger name"

    if(r.preCondition && (!r.preCondition.name || r.preCondition.name.length == 0))
      throw "Cannot add rule with null or empty preCondition name"
    if(r.postCondition && (!r.postCondition.name || r.postCondition.name.length == 0))
      throw "Cannot add rule with null or empty postCondition name"

		this.addConditions(r)
		this.store.putRule(r)
	}

	removeRule(name:string)
	{
		this.store.removeRule(name)
	}

	replaceValue(v:Value<any>) {
		this.store.putValue(v)
	}

	replaceCondition(c:Condition) {
		this.loadOrAddValues(c)
		this.store.putCondition(c)
	}

	replaceRule(r:Rule) {
		this.addConditions(r)
		this.store.putRule(r)
	}

	async processRule(rule:Rule) {
		if(this.locks[rule.name])
			return
		this.locks[rule.name] = true

		if (rule.preCondition)
			await rule.preCondition.makeTrue()

		const triggered = await rule.trigger.checkTrue()
		if (triggered) {
			await rule.action.makeTrue()
		}

		if (rule.postCondition)
			await rule.postCondition.makeTrue()

		this.locks[rule.name] = false
	}

	/**
	 * TODO change this to be based on an interrupt field in the rule:
	 *      E.g. a delay interrupt will execute the rule after a delay and then when
	 *      finished will schedule the next execution and so on.
	 *      This means that you simply start/init the rulebase and it will then schedule
	 *      all interrupts. The user will need to be able to define their own interrupts also.
	 *      Start/init will then replace processRules().
	 * @return true if the rules were executed, false otherwise.
	 */
	async processRules():Promise<boolean>
	{
		// Only run after the run period has elapsed:
		const now = new Date()
		if(this.lastRun && (now.getTime()-this.lastRun.getTime() < this.runPeriod))
			return false
		else
			this.lastRun = now

    await Promise.all(this.store.allRules().map((rule) => this.processRule(rule)))

		return true
	}

	start() {
		this.store.allRules().forEach( (rule:Rule) => {
			if(!rule.interrupt)
				rule.interrupt = new TimerInterrupt(this.runPeriod)
			rule.interrupt.start(this, rule)
		} )
	}

	stop() {
		this.store.allRules().forEach( (rule:Rule) => {
			if(rule.interrupt)
				rule.interrupt.stop(this, rule)
		} )
	}

	when(trigger:ConditionData):RuleBuilder
	{
		return new RuleBuilder(this, trigger)
	}

	allRules():Rule[]
	{
		return this.store.allRules()
	}

	allConditions():Condition[]
	{
		return this.store.allConditions()
	}

	allValues():Value<any>[]
	{
		return this.store.allValues()
	}

	/////////////////////
	// Factory methods //
  /////////////////////

	constantValue<T>(v:T):ConstantValue<T>&WithName<ConstantValue<T>>
	{
		const value = new ConstantValue<T>(v)
		return withName(value, v => this.addValue(v))
	}

	remoteValue<T>(remoteAddress:string, remoteValueName:string):RemoteValue<T>&WithName<RemoteValue<T>>
	{
		if(!this.remoteValueProtocol)
			throw "A RemoteValueProtocol must be assigned to the RuleBase prior to creating a RemoteValue object."
		const value = new RemoteValue<T>(this.remoteValueProtocol)
		value.remoteValueName = remoteValueName
		value.remoteAddress = remoteAddress
		return withName(value, v => this.addValue(v))
	}

	variableValue<T>(variableName:string = null):VariableValue<T>&WithName<VariableValue<T>>
	{
		if(!this.variableStore)
			throw "A VariableStore must be assigned to the RuleBase prior to creating a VariableValue object."
		if(variableName === null)
			variableName = name
		const value = new VariableValue<T>(this.variableStore)
		value.variableName = variableName
		return withName(value, v => this.addValue(v))
	}

	dateTimeValue():DateTimeValue&WithName<DateTimeValue> {
		const value =  new DateTimeValue()
		return withName(value, v => this.addValue(v))
	}

	logValue():LogValue&WithName<LogValue> {
		const value = new LogValue()
		return withName(value, v => this.addValue(v))
	}
}
