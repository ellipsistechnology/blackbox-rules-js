export default interface Value<T> {
	name:string;
	type:string;

	/**
	 * Gets the value of this Value object.
	 * @return A Promise that will resolve with the current value
	 */
	get():Promise<T>;

	/**
	 * Applies the given value to this Value object.
	 * @param v The value to apply
	 */
	set(v:T);
}
