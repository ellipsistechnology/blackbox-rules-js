import Condition from './Condition'
import Value from './Value'

/**
 * Condition types:
 *
 * Equals:          Left value must be equal to right value.
 * Less than:       Left value must be less than right value (trigger conditions only).
 * Greater than:    Left value must be greater than right value (trigger conditions only).
 * And:             Left value and right value are conditions both of which
 *                  must true for a trigger, both of which will be enforced if an action.
 * Or:              Left value and right value are conditions either of which
 *                  must be true (trigger conditions only).
 */

export interface ConditionData {
  name?:string
  left?:Value<any>
  right?:Value<any>
  type?:string
  template?:any
}

export default class ConditionImpl implements Condition, ConditionData {
  name:string
  left:Value<any>
  right:Value<any>
  type:string
  template:any

  constructor({left, right, name, type, template}:ConditionData = {}) {
    this.left = left
    this.right = right
    this.name = name
    this.type = type
    this.template = template
  }

// TODO try and move and and or into their own classes
  and(cond:Condition):Condition {
    // let c = new ConditionImpl({ left: this, right: cond, type: "and" })
    // c.checkTrue = async () => await c.left.get() && await c.right.get()
    // c.makeTrue = async () => {
    //   c.left.set(true);
    //   c.right.set(true);
    // }
    // return c
    return new AndCondition({ left: this, right: cond })
  }

  or(cond:Condition):Condition {
    // let c = new ConditionImpl({ left: this, right: cond, type: "or" })
    // c.checkTrue = async () => await c.left.get() || await c.right.get()
    // c.makeTrue = async () => {}
    // return c
    return new OrCondition({ left: this, right: cond })
  }

  async checkTrue():Promise<boolean> {
    throw new Error('Condition.checkTrue() must be implemented in subclass')
  }

  async makeTrue():Promise<void> {
    throw new Error('Condition.makeTrue() must be implemented in subclass')
  }

  async get():Promise<boolean> {
    return this.checkTrue()
  }

  set(v:boolean) {
    if(v)
      this.makeTrue().then()
  }
}

export class AndCondition extends ConditionImpl {
  constructor(props) {
    super(props)
    this.type = 'and'
  }

  async checkTrue():Promise<boolean> {
    return await this.left.get() && await this.right.get()
  }

  async makeTrue() {
    this.left.set(true);
    this.right.set(true);
  }
}

export class OrCondition extends ConditionImpl {
  constructor(props) {
    super(props)
    this.type = 'or'
  }

  async checkTrue() {
    return await this.left.get() || await this.right.get()
  }

  async makeTrue():Promise<void> {}
}
